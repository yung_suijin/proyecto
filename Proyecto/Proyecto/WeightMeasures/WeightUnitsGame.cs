﻿using Library;
using System.Windows.Forms;

namespace Proyecto.WeightMeasures
{
    public partial class WeightUnitsGame : Form
    {
        private readonly WindowEvents windowEvents;
        private readonly DragAndDropEvents dragAndDropEvents;
        private readonly WeightUnitsConversionsGame weightUnitsConversionsGame;
        private PictureBox[] images;
        private PictureBox selected;

        public WeightUnitsGame()
        {
            InitializeComponent();

            windowEvents = new WindowEvents();
            dragAndDropEvents = new DragAndDropEvents();
            weightUnitsConversionsGame = new WeightUnitsConversionsGame();

            images = new PictureBox[] { AnswerBoxSpider, AnswerBoxLake, AnswerBoxCar, AnswerBoxRing, AnswerBoxWaterGlass,
                AnswerBoxEggs, AnswerBoxApple, imgMilligram, imgGram, imgKilogram, imgMilliliter, imgLiter };

            imgMilligram.BackgroundImage = Proyecto.Properties.Resources.Milligram;
            imgGram.BackgroundImage = Proyecto.Properties.Resources.Gram;
            imgKilogram.BackgroundImage = Proyecto.Properties.Resources.Kilogram;
            imgMilliliter.BackgroundImage = Proyecto.Properties.Resources.Milliliter;
            imgLiter.BackgroundImage = Proyecto.Properties.Resources.Liter;

            foreach (var image in images)
            {
                image.AllowDrop = true;
                image.DragDrop += DragDropImage;
                image.DragEnter += dragAndDropEvents.DragEnterImage;
                image.MouseClick += ImageMouseClick;
                image.MouseMove += dragAndDropEvents.ImageMouseMove;
            }
        }

        private void DragDropImage(object sender, DragEventArgs e)
        {
            var answerBox = (PictureBox)sender;

            if (e.Data.GetDataPresent(typeof(PictureBox)))
            {
                var imageLengthUnit = (PictureBox)e.Data.GetData(typeof(PictureBox));

                if (imageLengthUnit != answerBox)
                {
                    dragAndDropEvents.DropAndCheck(imageLengthUnit, answerBox, this, weightUnitsConversionsGame);

                    selected = null;
                    dragAndDropEvents.SelectedImage(answerBox, images, selected);

                    return;
                }
            }
        }

        private void ImageMouseClick(object sender, MouseEventArgs e)
        {
            dragAndDropEvents.SelectedImage((PictureBox)sender, images, selected);
        }

        private void pctGameInstructionsButton_Click(object sender, System.EventArgs e)
        {
            windowEvents.MaintainCurrentWindowOpenAndOpenNextWindow(new DragDropGameInstructions());
        }
    }
}