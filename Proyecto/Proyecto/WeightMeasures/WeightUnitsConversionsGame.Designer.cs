﻿
namespace Proyecto.WeightMeasures
{
    partial class WeightUnitsConversionsGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WeightUnitsConversionsGame));
            this.txtBottleWeight = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCupWeight = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBirdWeight = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtElephantWeight = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTitleLengthExercises = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pctGameInstructionsButton = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.imgDog = new System.Windows.Forms.PictureBox();
            this.txtPillWeight = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.txtKidWeight = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.btnCheckKid = new System.Windows.Forms.Button();
            this.btnCheckPill = new System.Windows.Forms.Button();
            this.btnCheckBottle = new System.Windows.Forms.Button();
            this.btnCheckCup = new System.Windows.Forms.Button();
            this.btnCheckBird = new System.Windows.Forms.Button();
            this.btnCheckElephant = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctGameInstructionsButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // txtBottleWeight
            // 
            this.txtBottleWeight.Font = new System.Drawing.Font("Baloo", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBottleWeight.Location = new System.Drawing.Point(134, 607);
            this.txtBottleWeight.Name = "txtBottleWeight";
            this.txtBottleWeight.Size = new System.Drawing.Size(128, 41);
            this.txtBottleWeight.TabIndex = 40;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label7.Font = new System.Drawing.Font("Baloo", 16F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(19, 610);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 35);
            this.label7.TabIndex = 41;
            this.label7.Text = "Milliliters:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label8.Font = new System.Drawing.Font("Baloo", 16F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(128, 399);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 35);
            this.label8.TabIndex = 39;
            this.label8.Text = "1 Lt";
            // 
            // txtCupWeight
            // 
            this.txtCupWeight.Font = new System.Drawing.Font("Baloo", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCupWeight.Location = new System.Drawing.Point(658, 337);
            this.txtCupWeight.Name = "txtCupWeight";
            this.txtCupWeight.Size = new System.Drawing.Size(128, 41);
            this.txtCupWeight.TabIndex = 36;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label5.Font = new System.Drawing.Font("Baloo", 16F);
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(573, 341);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 35);
            this.label5.TabIndex = 37;
            this.label5.Text = "Liters:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label6.Font = new System.Drawing.Font("Baloo", 16F);
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(674, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 35);
            this.label6.TabIndex = 35;
            this.label6.Text = "300 ml";
            // 
            // txtBirdWeight
            // 
            this.txtBirdWeight.Font = new System.Drawing.Font("Baloo", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBirdWeight.Location = new System.Drawing.Point(413, 337);
            this.txtBirdWeight.Name = "txtBirdWeight";
            this.txtBirdWeight.Size = new System.Drawing.Size(128, 41);
            this.txtBirdWeight.TabIndex = 31;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label3.Font = new System.Drawing.Font("Baloo", 16F);
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(281, 341);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 35);
            this.label3.TabIndex = 32;
            this.label3.Text = "Milligrams:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label4.Font = new System.Drawing.Font("Baloo", 16F);
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(382, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 35);
            this.label4.TabIndex = 30;
            this.label4.Text = "40 g";
            // 
            // txtElephantWeight
            // 
            this.txtElephantWeight.Font = new System.Drawing.Font("Baloo", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtElephantWeight.Location = new System.Drawing.Point(112, 338);
            this.txtElephantWeight.Name = "txtElephantWeight";
            this.txtElephantWeight.Size = new System.Drawing.Size(128, 41);
            this.txtElephantWeight.TabIndex = 27;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Font = new System.Drawing.Font("Baloo", 16F);
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(19, 341);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 35);
            this.label2.TabIndex = 28;
            this.label2.Text = "Grams:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label1.Font = new System.Drawing.Font("Baloo", 16F);
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(97, 133);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 35);
            this.label1.TabIndex = 26;
            this.label1.Text = "90 Kg";
            // 
            // lblTitleLengthExercises
            // 
            this.lblTitleLengthExercises.AutoSize = true;
            this.lblTitleLengthExercises.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTitleLengthExercises.Font = new System.Drawing.Font("Baloo", 16F);
            this.lblTitleLengthExercises.Location = new System.Drawing.Point(17, 16);
            this.lblTitleLengthExercises.Name = "lblTitleLengthExercises";
            this.lblTitleLengthExercises.Size = new System.Drawing.Size(729, 105);
            this.lblTitleLengthExercises.TabIndex = 24;
            this.lblTitleLengthExercises.Text = "According to the previous explanations about conversions, write in the \r\nwhite bo" +
    "x the actual conversion for the weight that belongs to the \r\nobject (red weight " +
    "text):";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox3.Image = global::Proyecto.Properties.Resources.bottle;
            this.pictureBox3.Location = new System.Drawing.Point(62, 437);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(178, 154);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 38;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox2.Image = global::Proyecto.Properties.Resources.Cup;
            this.pictureBox2.Location = new System.Drawing.Point(621, 171);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(178, 154);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 34;
            this.pictureBox2.TabStop = false;
            // 
            // pctGameInstructionsButton
            // 
            this.pctGameInstructionsButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pctGameInstructionsButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pctGameInstructionsButton.Image = global::Proyecto.Properties.Resources.QuestionButton;
            this.pctGameInstructionsButton.Location = new System.Drawing.Point(774, 16);
            this.pctGameInstructionsButton.Name = "pctGameInstructionsButton";
            this.pctGameInstructionsButton.Size = new System.Drawing.Size(106, 77);
            this.pctGameInstructionsButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pctGameInstructionsButton.TabIndex = 33;
            this.pctGameInstructionsButton.TabStop = false;
            this.pctGameInstructionsButton.Click += new System.EventHandler(this.pctGameInstructionsButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox1.Image = global::Proyecto.Properties.Resources.Bird;
            this.pictureBox1.Location = new System.Drawing.Point(329, 171);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(178, 154);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 29;
            this.pictureBox1.TabStop = false;
            // 
            // imgDog
            // 
            this.imgDog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgDog.Image = global::Proyecto.Properties.Resources.Elephant;
            this.imgDog.Location = new System.Drawing.Point(44, 154);
            this.imgDog.Name = "imgDog";
            this.imgDog.Size = new System.Drawing.Size(178, 171);
            this.imgDog.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgDog.TabIndex = 25;
            this.imgDog.TabStop = false;
            // 
            // txtPillWeight
            // 
            this.txtPillWeight.Font = new System.Drawing.Font("Baloo", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPillWeight.Location = new System.Drawing.Point(398, 607);
            this.txtPillWeight.Name = "txtPillWeight";
            this.txtPillWeight.Size = new System.Drawing.Size(128, 41);
            this.txtPillWeight.TabIndex = 44;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label9.Font = new System.Drawing.Font("Baloo", 16F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(305, 610);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 35);
            this.label9.TabIndex = 45;
            this.label9.Text = "Grams:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label10.Font = new System.Drawing.Font("Baloo", 16F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(394, 399);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 35);
            this.label10.TabIndex = 43;
            this.label10.Text = "200 mg";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox4.Image = global::Proyecto.Properties.Resources.Pill;
            this.pictureBox4.Location = new System.Drawing.Point(348, 437);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(178, 154);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 42;
            this.pictureBox4.TabStop = false;
            // 
            // txtKidWeight
            // 
            this.txtKidWeight.Font = new System.Drawing.Font("Baloo", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKidWeight.Location = new System.Drawing.Point(719, 607);
            this.txtKidWeight.Name = "txtKidWeight";
            this.txtKidWeight.Size = new System.Drawing.Size(128, 41);
            this.txtKidWeight.TabIndex = 48;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label11.Font = new System.Drawing.Font("Baloo", 16F);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(596, 610);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(123, 35);
            this.label11.TabIndex = 49;
            this.label11.Text = "Kilograms:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label12.Font = new System.Drawing.Font("Baloo", 16F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(675, 399);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(92, 35);
            this.label12.TabIndex = 47;
            this.label12.Text = "25000 g";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox5.Image = global::Proyecto.Properties.Resources.Girl;
            this.pictureBox5.Location = new System.Drawing.Point(636, 437);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(178, 154);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 46;
            this.pictureBox5.TabStop = false;
            // 
            // btnCheckKid
            // 
            this.btnCheckKid.BackColor = System.Drawing.Color.LightGreen;
            this.btnCheckKid.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCheckKid.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCheckKid.Font = new System.Drawing.Font("Baloo", 12F);
            this.btnCheckKid.Location = new System.Drawing.Point(824, 487);
            this.btnCheckKid.Name = "btnCheckKid";
            this.btnCheckKid.Size = new System.Drawing.Size(70, 39);
            this.btnCheckKid.TabIndex = 55;
            this.btnCheckKid.Tag = "25";
            this.btnCheckKid.Text = "Check";
            this.btnCheckKid.UseVisualStyleBackColor = false;
            // 
            // btnCheckPill
            // 
            this.btnCheckPill.BackColor = System.Drawing.Color.LightGreen;
            this.btnCheckPill.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCheckPill.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCheckPill.Font = new System.Drawing.Font("Baloo", 12F);
            this.btnCheckPill.Location = new System.Drawing.Point(536, 487);
            this.btnCheckPill.Name = "btnCheckPill";
            this.btnCheckPill.Size = new System.Drawing.Size(70, 39);
            this.btnCheckPill.TabIndex = 54;
            this.btnCheckPill.Tag = "0.2";
            this.btnCheckPill.Text = "Check";
            this.btnCheckPill.UseVisualStyleBackColor = false;
            // 
            // btnCheckBottle
            // 
            this.btnCheckBottle.BackColor = System.Drawing.Color.LightGreen;
            this.btnCheckBottle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCheckBottle.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCheckBottle.Font = new System.Drawing.Font("Baloo", 12F);
            this.btnCheckBottle.Location = new System.Drawing.Point(249, 487);
            this.btnCheckBottle.Name = "btnCheckBottle";
            this.btnCheckBottle.Size = new System.Drawing.Size(70, 39);
            this.btnCheckBottle.TabIndex = 53;
            this.btnCheckBottle.Tag = "1000";
            this.btnCheckBottle.Text = "Check";
            this.btnCheckBottle.UseVisualStyleBackColor = false;
            // 
            // btnCheckCup
            // 
            this.btnCheckCup.BackColor = System.Drawing.Color.LightGreen;
            this.btnCheckCup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCheckCup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCheckCup.Font = new System.Drawing.Font("Baloo", 12F);
            this.btnCheckCup.Location = new System.Drawing.Point(810, 221);
            this.btnCheckCup.Name = "btnCheckCup";
            this.btnCheckCup.Size = new System.Drawing.Size(70, 39);
            this.btnCheckCup.TabIndex = 52;
            this.btnCheckCup.Tag = "0.3";
            this.btnCheckCup.Text = "Check";
            this.btnCheckCup.UseVisualStyleBackColor = false;
            // 
            // btnCheckBird
            // 
            this.btnCheckBird.BackColor = System.Drawing.Color.LightGreen;
            this.btnCheckBird.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCheckBird.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCheckBird.Font = new System.Drawing.Font("Baloo", 12F);
            this.btnCheckBird.Location = new System.Drawing.Point(515, 221);
            this.btnCheckBird.Name = "btnCheckBird";
            this.btnCheckBird.Size = new System.Drawing.Size(70, 39);
            this.btnCheckBird.TabIndex = 51;
            this.btnCheckBird.Tag = "40000";
            this.btnCheckBird.Text = "Check";
            this.btnCheckBird.UseVisualStyleBackColor = false;
            // 
            // btnCheckElephant
            // 
            this.btnCheckElephant.BackColor = System.Drawing.Color.LightGreen;
            this.btnCheckElephant.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCheckElephant.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCheckElephant.Font = new System.Drawing.Font("Baloo", 12F);
            this.btnCheckElephant.Location = new System.Drawing.Point(230, 221);
            this.btnCheckElephant.Name = "btnCheckElephant";
            this.btnCheckElephant.Size = new System.Drawing.Size(70, 39);
            this.btnCheckElephant.TabIndex = 50;
            this.btnCheckElephant.Tag = "90000";
            this.btnCheckElephant.Text = "Check";
            this.btnCheckElephant.UseVisualStyleBackColor = false;
            // 
            // WeightUnitsConversionsGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Proyecto.Properties.Resources.wall10;
            this.ClientSize = new System.Drawing.Size(915, 677);
            this.Controls.Add(this.btnCheckKid);
            this.Controls.Add(this.btnCheckPill);
            this.Controls.Add(this.btnCheckBottle);
            this.Controls.Add(this.btnCheckCup);
            this.Controls.Add(this.btnCheckBird);
            this.Controls.Add(this.btnCheckElephant);
            this.Controls.Add(this.txtKidWeight);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.txtPillWeight);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.txtBottleWeight);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.txtCupWeight);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pctGameInstructionsButton);
            this.Controls.Add(this.txtBirdWeight);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtElephantWeight);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imgDog);
            this.Controls.Add(this.lblTitleLengthExercises);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WeightUnitsConversionsGame";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Weight Units";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctGameInstructionsButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBottleWeight;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox txtCupWeight;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pctGameInstructionsButton;
        private System.Windows.Forms.TextBox txtBirdWeight;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtElephantWeight;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox imgDog;
        private System.Windows.Forms.Label lblTitleLengthExercises;
        private System.Windows.Forms.TextBox txtPillWeight;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TextBox txtKidWeight;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Button btnCheckKid;
        private System.Windows.Forms.Button btnCheckPill;
        private System.Windows.Forms.Button btnCheckBottle;
        private System.Windows.Forms.Button btnCheckCup;
        private System.Windows.Forms.Button btnCheckBird;
        private System.Windows.Forms.Button btnCheckElephant;
    }
}