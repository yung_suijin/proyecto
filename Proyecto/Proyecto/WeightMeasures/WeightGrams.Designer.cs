﻿
namespace Proyecto.WeightMeasures
{
    partial class WeightGrams
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WeightGrams));
            this.btnNextWeightLiters = new System.Windows.Forms.Button();
            this.btnBeforeWeightKilograms = new System.Windows.Forms.Button();
            this.imgLength6 = new System.Windows.Forms.PictureBox();
            this.imgLength5 = new System.Windows.Forms.PictureBox();
            this.imgLength4 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength4)).BeginInit();
            this.SuspendLayout();
            // 
            // btnNextWeightLiters
            // 
            this.btnNextWeightLiters.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnNextWeightLiters.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNextWeightLiters.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNextWeightLiters.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnNextWeightLiters.Location = new System.Drawing.Point(597, 355);
            this.btnNextWeightLiters.Name = "btnNextWeightLiters";
            this.btnNextWeightLiters.Size = new System.Drawing.Size(109, 60);
            this.btnNextWeightLiters.TabIndex = 12;
            this.btnNextWeightLiters.Text = "Next";
            this.btnNextWeightLiters.UseVisualStyleBackColor = false;
            this.btnNextWeightLiters.Click += new System.EventHandler(this.btnNextWeightLiters_Click);
            // 
            // btnBeforeWeightKilograms
            // 
            this.btnBeforeWeightKilograms.BackColor = System.Drawing.Color.LightGreen;
            this.btnBeforeWeightKilograms.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBeforeWeightKilograms.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBeforeWeightKilograms.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnBeforeWeightKilograms.Location = new System.Drawing.Point(471, 355);
            this.btnBeforeWeightKilograms.Name = "btnBeforeWeightKilograms";
            this.btnBeforeWeightKilograms.Size = new System.Drawing.Size(109, 60);
            this.btnBeforeWeightKilograms.TabIndex = 11;
            this.btnBeforeWeightKilograms.Text = "Before";
            this.btnBeforeWeightKilograms.UseVisualStyleBackColor = false;
            this.btnBeforeWeightKilograms.Click += new System.EventHandler(this.btnBeforeWeightKilograms_Click);
            // 
            // imgLength6
            // 
            this.imgLength6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength6.Image = global::Proyecto.Properties.Resources.GramSmallerParts;
            this.imgLength6.Location = new System.Drawing.Point(24, 331);
            this.imgLength6.Name = "imgLength6";
            this.imgLength6.Size = new System.Drawing.Size(428, 111);
            this.imgLength6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength6.TabIndex = 16;
            this.imgLength6.TabStop = false;
            // 
            // imgLength5
            // 
            this.imgLength5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength5.Image = global::Proyecto.Properties.Resources.GramScale;
            this.imgLength5.Location = new System.Drawing.Point(355, 2);
            this.imgLength5.Name = "imgLength5";
            this.imgLength5.Size = new System.Drawing.Size(351, 323);
            this.imgLength5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength5.TabIndex = 15;
            this.imgLength5.TabStop = false;
            // 
            // imgLength4
            // 
            this.imgLength4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength4.Image = global::Proyecto.Properties.Resources.GramInfo;
            this.imgLength4.Location = new System.Drawing.Point(24, 15);
            this.imgLength4.Name = "imgLength4";
            this.imgLength4.Size = new System.Drawing.Size(305, 297);
            this.imgLength4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength4.TabIndex = 14;
            this.imgLength4.TabStop = false;
            // 
            // WeightGrams
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Proyecto.Properties.Resources.wall15;
            this.ClientSize = new System.Drawing.Size(731, 450);
            this.Controls.Add(this.imgLength6);
            this.Controls.Add(this.imgLength5);
            this.Controls.Add(this.imgLength4);
            this.Controls.Add(this.btnNextWeightLiters);
            this.Controls.Add(this.btnBeforeWeightKilograms);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WeightGrams";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Weight Units";
            ((System.ComponentModel.ISupportInitialize)(this.imgLength6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnNextWeightLiters;
        private System.Windows.Forms.Button btnBeforeWeightKilograms;
        private System.Windows.Forms.PictureBox imgLength6;
        private System.Windows.Forms.PictureBox imgLength5;
        private System.Windows.Forms.PictureBox imgLength4;
    }
}