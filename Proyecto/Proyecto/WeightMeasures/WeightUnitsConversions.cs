﻿using Library;
using System;
using System.Windows.Forms;

namespace Proyecto.WeightMeasures
{
    public partial class WeightUnitsConversions : Form
    {
        private readonly WindowEvents windowEvents;

        public WeightUnitsConversions()
        {
            InitializeComponent();

            windowEvents = new WindowEvents();
        }

        private void btnBeforeLiters_Click(object sender, EventArgs e)
        {
            windowEvents.CloseCurrentWindowAndOpenNextWindow(this, new WeightLiters());
        }

        private void btnNextWeightConversions_Click(object sender, EventArgs e)
        {
            windowEvents.CloseCurrentWindowAndOpenNextWindow(this, new WeightUnitsGame());
        }
    }
}