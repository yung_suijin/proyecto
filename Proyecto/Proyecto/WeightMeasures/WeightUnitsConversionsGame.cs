﻿using Library;
using System;
using System.Windows.Forms;

namespace Proyecto.WeightMeasures
{
    public partial class WeightUnitsConversionsGame : Form
    {
        private readonly WindowEvents windowEvents;
        private readonly TextValidator textValidator;
        private readonly TextBoxAnswersEvents textBoxAnswersEvents;
        private readonly FinishedUnit finishedUnit;
        private TextBox[] answerBoxes;
        private Button[] checkButtons;

        public WeightUnitsConversionsGame()
        {
            InitializeComponent();

            windowEvents = new WindowEvents();
            textValidator = new TextValidator();
            textBoxAnswersEvents = new TextBoxAnswersEvents();
            finishedUnit = new FinishedUnit();

            answerBoxes = new TextBox[] { txtElephantWeight, txtBirdWeight, txtCupWeight, txtBottleWeight, txtPillWeight,
                txtKidWeight };

            checkButtons = new Button[] { btnCheckElephant, btnCheckBird, btnCheckCup, btnCheckBottle, btnCheckPill,
                btnCheckKid };

            foreach (var txtBox in answerBoxes)
            {
                txtBox.TextChanged += textValidator.ValidateNumberInTextBox;
            }

            foreach (var button in checkButtons)
            {
                button.Click += CheckAnswers;
            }
        }

        private void CheckAnswers(object sender, EventArgs e)
        {
            var clickedButton = (Button)sender;

            textBoxAnswersEvents.CheckTextBoxAnswers(checkButtons, clickedButton, answerBoxes, this, finishedUnit);
        }

        private void pctGameInstructionsButton_Click(object sender, EventArgs e)
        {
            windowEvents.MaintainCurrentWindowOpenAndOpenNextWindow(new AnswersGameInstructions());
        }
    }
}