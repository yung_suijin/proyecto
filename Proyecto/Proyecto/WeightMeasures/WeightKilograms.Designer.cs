﻿
namespace Proyecto.WeightMeasures
{
    partial class WeightKilograms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WeightKilograms));
            this.btnNextWeightGram = new System.Windows.Forms.Button();
            this.btnBeforeWeights = new System.Windows.Forms.Button();
            this.imgLength6 = new System.Windows.Forms.PictureBox();
            this.imgLength5 = new System.Windows.Forms.PictureBox();
            this.imgLength4 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength4)).BeginInit();
            this.SuspendLayout();
            // 
            // btnNextWeightGram
            // 
            this.btnNextWeightGram.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnNextWeightGram.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNextWeightGram.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNextWeightGram.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnNextWeightGram.Location = new System.Drawing.Point(597, 355);
            this.btnNextWeightGram.Name = "btnNextWeightGram";
            this.btnNextWeightGram.Size = new System.Drawing.Size(109, 60);
            this.btnNextWeightGram.TabIndex = 10;
            this.btnNextWeightGram.Text = "Next";
            this.btnNextWeightGram.UseVisualStyleBackColor = false;
            this.btnNextWeightGram.Click += new System.EventHandler(this.btnNextWeightGram_Click);
            // 
            // btnBeforeWeights
            // 
            this.btnBeforeWeights.BackColor = System.Drawing.Color.LightGreen;
            this.btnBeforeWeights.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBeforeWeights.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBeforeWeights.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnBeforeWeights.Location = new System.Drawing.Point(471, 355);
            this.btnBeforeWeights.Name = "btnBeforeWeights";
            this.btnBeforeWeights.Size = new System.Drawing.Size(109, 60);
            this.btnBeforeWeights.TabIndex = 9;
            this.btnBeforeWeights.Text = "Before";
            this.btnBeforeWeights.UseVisualStyleBackColor = false;
            this.btnBeforeWeights.Click += new System.EventHandler(this.btnBeforeMeasures_Click);
            // 
            // imgLength6
            // 
            this.imgLength6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength6.Image = global::Proyecto.Properties.Resources.KilogramSmallerParts;
            this.imgLength6.Location = new System.Drawing.Point(24, 334);
            this.imgLength6.Name = "imgLength6";
            this.imgLength6.Size = new System.Drawing.Size(428, 111);
            this.imgLength6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength6.TabIndex = 13;
            this.imgLength6.TabStop = false;
            // 
            // imgLength5
            // 
            this.imgLength5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength5.Image = global::Proyecto.Properties.Resources.KilogramScale;
            this.imgLength5.Location = new System.Drawing.Point(355, 5);
            this.imgLength5.Name = "imgLength5";
            this.imgLength5.Size = new System.Drawing.Size(351, 323);
            this.imgLength5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength5.TabIndex = 12;
            this.imgLength5.TabStop = false;
            // 
            // imgLength4
            // 
            this.imgLength4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength4.Image = global::Proyecto.Properties.Resources.KilogramInfo;
            this.imgLength4.Location = new System.Drawing.Point(24, 18);
            this.imgLength4.Name = "imgLength4";
            this.imgLength4.Size = new System.Drawing.Size(305, 297);
            this.imgLength4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength4.TabIndex = 11;
            this.imgLength4.TabStop = false;
            // 
            // WeightKilograms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Proyecto.Properties.Resources.wall13;
            this.ClientSize = new System.Drawing.Size(731, 450);
            this.Controls.Add(this.imgLength6);
            this.Controls.Add(this.imgLength5);
            this.Controls.Add(this.imgLength4);
            this.Controls.Add(this.btnNextWeightGram);
            this.Controls.Add(this.btnBeforeWeights);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WeightKilograms";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Weight Units";
            ((System.ComponentModel.ISupportInitialize)(this.imgLength6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnNextWeightGram;
        private System.Windows.Forms.Button btnBeforeWeights;
        private System.Windows.Forms.PictureBox imgLength6;
        private System.Windows.Forms.PictureBox imgLength5;
        private System.Windows.Forms.PictureBox imgLength4;
    }
}