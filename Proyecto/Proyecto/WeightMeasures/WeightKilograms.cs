﻿using Library;
using System;
using System.Windows.Forms;

namespace Proyecto.WeightMeasures
{
    public partial class WeightKilograms : Form
    {
        private readonly WindowEvents buttonEvents;

        public WeightKilograms()
        {
            InitializeComponent();

            buttonEvents = new WindowEvents();
        }

        private void btnBeforeMeasures_Click(object sender, EventArgs e)
        {
            buttonEvents.CloseCurrentWindowAndOpenNextWindow(this, new Weights());
        }

        private void btnNextWeightGram_Click(object sender, EventArgs e)
        {
            buttonEvents.CloseCurrentWindowAndOpenNextWindow(this, new WeightGrams());
        }
    }
}