﻿using Library;
using Proyecto.WeightMeasures;
using System;
using System.Windows.Forms;

namespace Proyecto
{
    public partial class Weights : Form
    {
        private readonly WindowEvents buttonEvents;

        public Weights()
        {
            InitializeComponent();

            buttonEvents = new WindowEvents();
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            buttonEvents.CloseCurrentWindowAndOpenNextWindow(this, new Menu());
        }

        private void btnNextWeightKilograms_Click(object sender, EventArgs e)
        {
            buttonEvents.CloseCurrentWindowAndOpenNextWindow(this, new WeightKilograms());
        }
    }
}