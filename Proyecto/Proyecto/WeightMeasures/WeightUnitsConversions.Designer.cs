﻿
namespace Proyecto.WeightMeasures
{
    partial class WeightUnitsConversions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WeightUnitsConversions));
            this.label1 = new System.Windows.Forms.Label();
            this.lblTitleConversions = new System.Windows.Forms.Label();
            this.btnNextWeightConversions = new System.Windows.Forms.Button();
            this.btnBeforeLiters = new System.Windows.Forms.Button();
            this.imgLength15 = new System.Windows.Forms.PictureBox();
            this.imgLength14 = new System.Windows.Forms.PictureBox();
            this.imgLength13 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength13)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label1.Font = new System.Drawing.Font("Baloo", 20F);
            this.label1.Location = new System.Drawing.Point(170, 218);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 42);
            this.label1.TabIndex = 11;
            this.label1.Text = "Examples:";
            // 
            // lblTitleConversions
            // 
            this.lblTitleConversions.AutoSize = true;
            this.lblTitleConversions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTitleConversions.Font = new System.Drawing.Font("Baloo", 20F);
            this.lblTitleConversions.Location = new System.Drawing.Point(172, 11);
            this.lblTitleConversions.Name = "lblTitleConversions";
            this.lblTitleConversions.Size = new System.Drawing.Size(172, 42);
            this.lblTitleConversions.TabIndex = 8;
            this.lblTitleConversions.Text = "Conversions";
            // 
            // btnNextWeightConversions
            // 
            this.btnNextWeightConversions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnNextWeightConversions.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNextWeightConversions.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNextWeightConversions.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnNextWeightConversions.Location = new System.Drawing.Point(659, 415);
            this.btnNextWeightConversions.Name = "btnNextWeightConversions";
            this.btnNextWeightConversions.Size = new System.Drawing.Size(109, 60);
            this.btnNextWeightConversions.TabIndex = 14;
            this.btnNextWeightConversions.Text = "Next";
            this.btnNextWeightConversions.UseVisualStyleBackColor = false;
            this.btnNextWeightConversions.Click += new System.EventHandler(this.btnNextWeightConversions_Click);
            // 
            // btnBeforeLiters
            // 
            this.btnBeforeLiters.BackColor = System.Drawing.Color.LightGreen;
            this.btnBeforeLiters.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBeforeLiters.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBeforeLiters.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnBeforeLiters.Location = new System.Drawing.Point(533, 415);
            this.btnBeforeLiters.Name = "btnBeforeLiters";
            this.btnBeforeLiters.Size = new System.Drawing.Size(109, 60);
            this.btnBeforeLiters.TabIndex = 13;
            this.btnBeforeLiters.Text = "Before";
            this.btnBeforeLiters.UseVisualStyleBackColor = false;
            this.btnBeforeLiters.Click += new System.EventHandler(this.btnBeforeLiters_Click);
            // 
            // imgLength15
            // 
            this.imgLength15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength15.Image = global::Proyecto.Properties.Resources.WeightUnitsConversions;
            this.imgLength15.Location = new System.Drawing.Point(-10, 263);
            this.imgLength15.Name = "imgLength15";
            this.imgLength15.Size = new System.Drawing.Size(546, 211);
            this.imgLength15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength15.TabIndex = 12;
            this.imgLength15.TabStop = false;
            // 
            // imgLength14
            // 
            this.imgLength14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength14.Image = global::Proyecto.Properties.Resources.WeightConversions;
            this.imgLength14.Location = new System.Drawing.Point(521, 14);
            this.imgLength14.Name = "imgLength14";
            this.imgLength14.Size = new System.Drawing.Size(259, 384);
            this.imgLength14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength14.TabIndex = 10;
            this.imgLength14.TabStop = false;
            // 
            // imgLength13
            // 
            this.imgLength13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength13.Image = global::Proyecto.Properties.Resources.WeightSmallerParts;
            this.imgLength13.Location = new System.Drawing.Point(23, 56);
            this.imgLength13.Name = "imgLength13";
            this.imgLength13.Size = new System.Drawing.Size(473, 139);
            this.imgLength13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength13.TabIndex = 9;
            this.imgLength13.TabStop = false;
            // 
            // WeightUnitsConversions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Proyecto.Properties.Resources.wall17;
            this.ClientSize = new System.Drawing.Size(800, 496);
            this.Controls.Add(this.btnNextWeightConversions);
            this.Controls.Add(this.btnBeforeLiters);
            this.Controls.Add(this.imgLength15);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imgLength14);
            this.Controls.Add(this.imgLength13);
            this.Controls.Add(this.lblTitleConversions);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WeightUnitsConversions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Weight Units";
            ((System.ComponentModel.ISupportInitialize)(this.imgLength15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength13)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox imgLength15;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox imgLength14;
        private System.Windows.Forms.PictureBox imgLength13;
        private System.Windows.Forms.Label lblTitleConversions;
        private System.Windows.Forms.Button btnNextWeightConversions;
        private System.Windows.Forms.Button btnBeforeLiters;
    }
}