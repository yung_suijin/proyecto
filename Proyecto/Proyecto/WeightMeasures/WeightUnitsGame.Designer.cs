﻿
namespace Proyecto.WeightMeasures
{
    partial class WeightUnitsGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WeightUnitsGame));
            this.lblTitleWeightExercises = new System.Windows.Forms.Label();
            this.pctGameInstructionsButton = new System.Windows.Forms.PictureBox();
            this.imgMilliliter = new System.Windows.Forms.PictureBox();
            this.AnswerBoxApple = new System.Windows.Forms.PictureBox();
            this.AnswerBoxEggs = new System.Windows.Forms.PictureBox();
            this.AnswerBoxWaterGlass = new System.Windows.Forms.PictureBox();
            this.AnswerBoxRing = new System.Windows.Forms.PictureBox();
            this.AnswerBoxCar = new System.Windows.Forms.PictureBox();
            this.AnswerBoxLake = new System.Windows.Forms.PictureBox();
            this.AnswerBoxSpider = new System.Windows.Forms.PictureBox();
            this.imgRing = new System.Windows.Forms.PictureBox();
            this.imgEggs = new System.Windows.Forms.PictureBox();
            this.imgWaterGlass = new System.Windows.Forms.PictureBox();
            this.imgApple = new System.Windows.Forms.PictureBox();
            this.imgLiter = new System.Windows.Forms.PictureBox();
            this.imgMilligram = new System.Windows.Forms.PictureBox();
            this.imgGram = new System.Windows.Forms.PictureBox();
            this.imgKilogram = new System.Windows.Forms.PictureBox();
            this.imgCar = new System.Windows.Forms.PictureBox();
            this.imgLake = new System.Windows.Forms.PictureBox();
            this.imgSpider = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctGameInstructionsButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMilliliter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxApple)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxEggs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxWaterGlass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxRing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxCar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxLake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxSpider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgRing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEggs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgWaterGlass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgApple)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLiter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMilligram)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgGram)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgKilogram)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSpider)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitleWeightExercises
            // 
            this.lblTitleWeightExercises.AutoSize = true;
            this.lblTitleWeightExercises.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTitleWeightExercises.Font = new System.Drawing.Font("Baloo", 16F);
            this.lblTitleWeightExercises.Location = new System.Drawing.Point(14, 15);
            this.lblTitleWeightExercises.Name = "lblTitleWeightExercises";
            this.lblTitleWeightExercises.Size = new System.Drawing.Size(728, 70);
            this.lblTitleWeightExercises.TabIndex = 24;
            this.lblTitleWeightExercises.Text = "According to the previous explanations about weights, drag the weight \r\ntype imag" +
    "e that you think it can measure weight for different things:";
            // 
            // pctGameInstructionsButton
            // 
            this.pctGameInstructionsButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pctGameInstructionsButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pctGameInstructionsButton.Image = global::Proyecto.Properties.Resources.QuestionButton;
            this.pctGameInstructionsButton.Location = new System.Drawing.Point(748, 15);
            this.pctGameInstructionsButton.Name = "pctGameInstructionsButton";
            this.pctGameInstructionsButton.Size = new System.Drawing.Size(100, 63);
            this.pctGameInstructionsButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pctGameInstructionsButton.TabIndex = 44;
            this.pctGameInstructionsButton.TabStop = false;
            this.pctGameInstructionsButton.Click += new System.EventHandler(this.pctGameInstructionsButton_Click);
            // 
            // imgMilliliter
            // 
            this.imgMilliliter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgMilliliter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.imgMilliliter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgMilliliter.Location = new System.Drawing.Point(689, 602);
            this.imgMilliliter.Name = "imgMilliliter";
            this.imgMilliliter.Size = new System.Drawing.Size(139, 33);
            this.imgMilliliter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgMilliliter.TabIndex = 43;
            this.imgMilliliter.TabStop = false;
            this.imgMilliliter.Tag = "Milliliter";
            // 
            // AnswerBoxApple
            // 
            this.AnswerBoxApple.BackColor = System.Drawing.Color.White;
            this.AnswerBoxApple.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AnswerBoxApple.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnswerBoxApple.Location = new System.Drawing.Point(662, 530);
            this.AnswerBoxApple.Name = "AnswerBoxApple";
            this.AnswerBoxApple.Size = new System.Drawing.Size(168, 39);
            this.AnswerBoxApple.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AnswerBoxApple.TabIndex = 42;
            this.AnswerBoxApple.TabStop = false;
            this.AnswerBoxApple.Tag = "Gram";
            // 
            // AnswerBoxEggs
            // 
            this.AnswerBoxEggs.BackColor = System.Drawing.Color.White;
            this.AnswerBoxEggs.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AnswerBoxEggs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnswerBoxEggs.Location = new System.Drawing.Point(460, 530);
            this.AnswerBoxEggs.Name = "AnswerBoxEggs";
            this.AnswerBoxEggs.Size = new System.Drawing.Size(168, 39);
            this.AnswerBoxEggs.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AnswerBoxEggs.TabIndex = 41;
            this.AnswerBoxEggs.TabStop = false;
            this.AnswerBoxEggs.Tag = "Kilogram";
            // 
            // AnswerBoxWaterGlass
            // 
            this.AnswerBoxWaterGlass.BackColor = System.Drawing.Color.White;
            this.AnswerBoxWaterGlass.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AnswerBoxWaterGlass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnswerBoxWaterGlass.Location = new System.Drawing.Point(244, 530);
            this.AnswerBoxWaterGlass.Name = "AnswerBoxWaterGlass";
            this.AnswerBoxWaterGlass.Size = new System.Drawing.Size(168, 39);
            this.AnswerBoxWaterGlass.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AnswerBoxWaterGlass.TabIndex = 40;
            this.AnswerBoxWaterGlass.TabStop = false;
            this.AnswerBoxWaterGlass.Tag = "Milliliter";
            // 
            // AnswerBoxRing
            // 
            this.AnswerBoxRing.BackColor = System.Drawing.Color.White;
            this.AnswerBoxRing.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AnswerBoxRing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnswerBoxRing.Location = new System.Drawing.Point(37, 530);
            this.AnswerBoxRing.Name = "AnswerBoxRing";
            this.AnswerBoxRing.Size = new System.Drawing.Size(168, 39);
            this.AnswerBoxRing.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AnswerBoxRing.TabIndex = 39;
            this.AnswerBoxRing.TabStop = false;
            this.AnswerBoxRing.Tag = "Gram";
            // 
            // AnswerBoxCar
            // 
            this.AnswerBoxCar.BackColor = System.Drawing.Color.White;
            this.AnswerBoxCar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AnswerBoxCar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnswerBoxCar.Location = new System.Drawing.Point(608, 287);
            this.AnswerBoxCar.Name = "AnswerBoxCar";
            this.AnswerBoxCar.Size = new System.Drawing.Size(168, 39);
            this.AnswerBoxCar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AnswerBoxCar.TabIndex = 38;
            this.AnswerBoxCar.TabStop = false;
            this.AnswerBoxCar.Tag = "Kilogram";
            // 
            // AnswerBoxLake
            // 
            this.AnswerBoxLake.BackColor = System.Drawing.Color.White;
            this.AnswerBoxLake.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AnswerBoxLake.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnswerBoxLake.Location = new System.Drawing.Point(330, 287);
            this.AnswerBoxLake.Name = "AnswerBoxLake";
            this.AnswerBoxLake.Size = new System.Drawing.Size(168, 39);
            this.AnswerBoxLake.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AnswerBoxLake.TabIndex = 37;
            this.AnswerBoxLake.TabStop = false;
            this.AnswerBoxLake.Tag = "Liter";
            // 
            // AnswerBoxSpider
            // 
            this.AnswerBoxSpider.BackColor = System.Drawing.Color.White;
            this.AnswerBoxSpider.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AnswerBoxSpider.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnswerBoxSpider.Location = new System.Drawing.Point(68, 287);
            this.AnswerBoxSpider.Name = "AnswerBoxSpider";
            this.AnswerBoxSpider.Size = new System.Drawing.Size(168, 39);
            this.AnswerBoxSpider.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AnswerBoxSpider.TabIndex = 36;
            this.AnswerBoxSpider.TabStop = false;
            this.AnswerBoxSpider.Tag = "Milligram";
            // 
            // imgRing
            // 
            this.imgRing.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgRing.Image = global::Proyecto.Properties.Resources.Ring;
            this.imgRing.Location = new System.Drawing.Point(37, 355);
            this.imgRing.Name = "imgRing";
            this.imgRing.Size = new System.Drawing.Size(168, 160);
            this.imgRing.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgRing.TabIndex = 35;
            this.imgRing.TabStop = false;
            // 
            // imgEggs
            // 
            this.imgEggs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgEggs.Image = global::Proyecto.Properties.Resources.Eggs;
            this.imgEggs.Location = new System.Drawing.Point(460, 355);
            this.imgEggs.Name = "imgEggs";
            this.imgEggs.Size = new System.Drawing.Size(163, 160);
            this.imgEggs.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgEggs.TabIndex = 34;
            this.imgEggs.TabStop = false;
            // 
            // imgWaterGlass
            // 
            this.imgWaterGlass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgWaterGlass.Image = global::Proyecto.Properties.Resources.WaterGlass;
            this.imgWaterGlass.Location = new System.Drawing.Point(244, 355);
            this.imgWaterGlass.Name = "imgWaterGlass";
            this.imgWaterGlass.Size = new System.Drawing.Size(170, 160);
            this.imgWaterGlass.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgWaterGlass.TabIndex = 33;
            this.imgWaterGlass.TabStop = false;
            // 
            // imgApple
            // 
            this.imgApple.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgApple.Image = global::Proyecto.Properties.Resources.Apple;
            this.imgApple.Location = new System.Drawing.Point(662, 355);
            this.imgApple.Name = "imgApple";
            this.imgApple.Size = new System.Drawing.Size(166, 160);
            this.imgApple.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgApple.TabIndex = 32;
            this.imgApple.TabStop = false;
            // 
            // imgLiter
            // 
            this.imgLiter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLiter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.imgLiter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgLiter.Location = new System.Drawing.Point(518, 602);
            this.imgLiter.Name = "imgLiter";
            this.imgLiter.Size = new System.Drawing.Size(145, 33);
            this.imgLiter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLiter.TabIndex = 31;
            this.imgLiter.TabStop = false;
            this.imgLiter.Tag = "Liter";
            // 
            // imgMilligram
            // 
            this.imgMilligram.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgMilligram.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.imgMilligram.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgMilligram.Location = new System.Drawing.Point(349, 602);
            this.imgMilligram.Name = "imgMilligram";
            this.imgMilligram.Size = new System.Drawing.Size(148, 33);
            this.imgMilligram.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgMilligram.TabIndex = 30;
            this.imgMilligram.TabStop = false;
            this.imgMilligram.Tag = "Milligram";
            // 
            // imgGram
            // 
            this.imgGram.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgGram.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.imgGram.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgGram.Location = new System.Drawing.Point(195, 602);
            this.imgGram.Name = "imgGram";
            this.imgGram.Size = new System.Drawing.Size(135, 33);
            this.imgGram.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgGram.TabIndex = 29;
            this.imgGram.TabStop = false;
            this.imgGram.Tag = "Gram";
            // 
            // imgKilogram
            // 
            this.imgKilogram.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgKilogram.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.imgKilogram.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgKilogram.Location = new System.Drawing.Point(36, 602);
            this.imgKilogram.Name = "imgKilogram";
            this.imgKilogram.Size = new System.Drawing.Size(142, 33);
            this.imgKilogram.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgKilogram.TabIndex = 28;
            this.imgKilogram.TabStop = false;
            this.imgKilogram.Tag = "Kilogram";
            // 
            // imgCar
            // 
            this.imgCar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgCar.Image = global::Proyecto.Properties.Resources.Car;
            this.imgCar.Location = new System.Drawing.Point(587, 114);
            this.imgCar.Name = "imgCar";
            this.imgCar.Size = new System.Drawing.Size(200, 141);
            this.imgCar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgCar.TabIndex = 27;
            this.imgCar.TabStop = false;
            // 
            // imgLake
            // 
            this.imgLake.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLake.Image = global::Proyecto.Properties.Resources.Lake;
            this.imgLake.Location = new System.Drawing.Point(316, 114);
            this.imgLake.Name = "imgLake";
            this.imgLake.Size = new System.Drawing.Size(199, 143);
            this.imgLake.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLake.TabIndex = 26;
            this.imgLake.TabStop = false;
            // 
            // imgSpider
            // 
            this.imgSpider.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgSpider.Image = global::Proyecto.Properties.Resources.Spider;
            this.imgSpider.Location = new System.Drawing.Point(68, 112);
            this.imgSpider.Name = "imgSpider";
            this.imgSpider.Size = new System.Drawing.Size(168, 143);
            this.imgSpider.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgSpider.TabIndex = 25;
            this.imgSpider.TabStop = false;
            // 
            // WeightUnitsGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Proyecto.Properties.Resources.wall9;
            this.ClientSize = new System.Drawing.Size(862, 656);
            this.Controls.Add(this.pctGameInstructionsButton);
            this.Controls.Add(this.imgMilliliter);
            this.Controls.Add(this.AnswerBoxApple);
            this.Controls.Add(this.AnswerBoxEggs);
            this.Controls.Add(this.AnswerBoxWaterGlass);
            this.Controls.Add(this.AnswerBoxRing);
            this.Controls.Add(this.AnswerBoxCar);
            this.Controls.Add(this.AnswerBoxLake);
            this.Controls.Add(this.AnswerBoxSpider);
            this.Controls.Add(this.imgRing);
            this.Controls.Add(this.imgEggs);
            this.Controls.Add(this.imgWaterGlass);
            this.Controls.Add(this.imgApple);
            this.Controls.Add(this.imgLiter);
            this.Controls.Add(this.imgMilligram);
            this.Controls.Add(this.imgGram);
            this.Controls.Add(this.imgKilogram);
            this.Controls.Add(this.imgCar);
            this.Controls.Add(this.imgLake);
            this.Controls.Add(this.imgSpider);
            this.Controls.Add(this.lblTitleWeightExercises);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WeightUnitsGame";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Weight Units";
            ((System.ComponentModel.ISupportInitialize)(this.pctGameInstructionsButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMilliliter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxApple)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxEggs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxWaterGlass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxRing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxCar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxLake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxSpider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgRing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEggs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgWaterGlass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgApple)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLiter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMilligram)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgGram)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgKilogram)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSpider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox AnswerBoxApple;
        private System.Windows.Forms.PictureBox AnswerBoxEggs;
        private System.Windows.Forms.PictureBox AnswerBoxWaterGlass;
        private System.Windows.Forms.PictureBox AnswerBoxRing;
        private System.Windows.Forms.PictureBox AnswerBoxCar;
        private System.Windows.Forms.PictureBox AnswerBoxLake;
        private System.Windows.Forms.PictureBox AnswerBoxSpider;
        private System.Windows.Forms.PictureBox imgRing;
        private System.Windows.Forms.PictureBox imgEggs;
        private System.Windows.Forms.PictureBox imgWaterGlass;
        private System.Windows.Forms.PictureBox imgApple;
        private System.Windows.Forms.PictureBox imgLiter;
        private System.Windows.Forms.PictureBox imgMilligram;
        private System.Windows.Forms.PictureBox imgGram;
        private System.Windows.Forms.PictureBox imgKilogram;
        private System.Windows.Forms.PictureBox imgCar;
        private System.Windows.Forms.PictureBox imgLake;
        private System.Windows.Forms.PictureBox imgSpider;
        private System.Windows.Forms.Label lblTitleWeightExercises;
        private System.Windows.Forms.PictureBox imgMilliliter;
        private System.Windows.Forms.PictureBox pctGameInstructionsButton;
    }
}