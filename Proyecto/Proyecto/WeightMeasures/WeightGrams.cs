﻿using Library;
using System;
using System.Windows.Forms;

namespace Proyecto.WeightMeasures
{
    public partial class WeightGrams : Form
    {
        private readonly WindowEvents buttonEvents;

        public WeightGrams()
        {
            InitializeComponent();

            buttonEvents = new WindowEvents();
        }

        private void btnBeforeWeightKilograms_Click(object sender, EventArgs e)
        {
            buttonEvents.CloseCurrentWindowAndOpenNextWindow(this, new WeightKilograms());
        }

        private void btnNextWeightLiters_Click(object sender, EventArgs e)
        {
            buttonEvents.CloseCurrentWindowAndOpenNextWindow(this, new WeightLiters());
        }
    }
}