﻿using Library;
using System;
using System.Windows.Forms;

namespace Proyecto.WeightMeasures
{
    public partial class WeightLiters : Form
    {
        private readonly WindowEvents buttonEvents;

        public WeightLiters()
        {
            InitializeComponent();

            buttonEvents = new WindowEvents();
        }

        private void btnBeforeWeightGrams_Click(object sender, EventArgs e)
        {
            buttonEvents.CloseCurrentWindowAndOpenNextWindow(this, new WeightGrams());
        }

        private void btnNextWeighConversions_Click(object sender, EventArgs e)
        {
            buttonEvents.CloseCurrentWindowAndOpenNextWindow(this, new WeightUnitsConversions());
        }
    }
}