﻿
namespace Proyecto.WeightMeasures
{
    partial class WeightLiters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WeightLiters));
            this.btnNextWeighConversions = new System.Windows.Forms.Button();
            this.btnBeforeWeightGrams = new System.Windows.Forms.Button();
            this.imgLength6 = new System.Windows.Forms.PictureBox();
            this.imgLength5 = new System.Windows.Forms.PictureBox();
            this.imgLength4 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength4)).BeginInit();
            this.SuspendLayout();
            // 
            // btnNextWeighConversions
            // 
            this.btnNextWeighConversions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnNextWeighConversions.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNextWeighConversions.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNextWeighConversions.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnNextWeighConversions.Location = new System.Drawing.Point(597, 355);
            this.btnNextWeighConversions.Name = "btnNextWeighConversions";
            this.btnNextWeighConversions.Size = new System.Drawing.Size(109, 60);
            this.btnNextWeighConversions.TabIndex = 14;
            this.btnNextWeighConversions.Text = "Next";
            this.btnNextWeighConversions.UseVisualStyleBackColor = false;
            this.btnNextWeighConversions.Click += new System.EventHandler(this.btnNextWeighConversions_Click);
            // 
            // btnBeforeWeightGrams
            // 
            this.btnBeforeWeightGrams.BackColor = System.Drawing.Color.LightGreen;
            this.btnBeforeWeightGrams.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBeforeWeightGrams.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBeforeWeightGrams.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnBeforeWeightGrams.Location = new System.Drawing.Point(471, 355);
            this.btnBeforeWeightGrams.Name = "btnBeforeWeightGrams";
            this.btnBeforeWeightGrams.Size = new System.Drawing.Size(109, 60);
            this.btnBeforeWeightGrams.TabIndex = 13;
            this.btnBeforeWeightGrams.Text = "Before";
            this.btnBeforeWeightGrams.UseVisualStyleBackColor = false;
            this.btnBeforeWeightGrams.Click += new System.EventHandler(this.btnBeforeWeightGrams_Click);
            // 
            // imgLength6
            // 
            this.imgLength6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength6.Image = global::Proyecto.Properties.Resources.LiterSmallerParts;
            this.imgLength6.Location = new System.Drawing.Point(27, 332);
            this.imgLength6.Name = "imgLength6";
            this.imgLength6.Size = new System.Drawing.Size(428, 111);
            this.imgLength6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength6.TabIndex = 19;
            this.imgLength6.TabStop = false;
            // 
            // imgLength5
            // 
            this.imgLength5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength5.Image = global::Proyecto.Properties.Resources.LiterScale;
            this.imgLength5.Location = new System.Drawing.Point(358, 3);
            this.imgLength5.Name = "imgLength5";
            this.imgLength5.Size = new System.Drawing.Size(351, 323);
            this.imgLength5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength5.TabIndex = 18;
            this.imgLength5.TabStop = false;
            // 
            // imgLength4
            // 
            this.imgLength4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength4.Image = global::Proyecto.Properties.Resources.LiterInfo;
            this.imgLength4.Location = new System.Drawing.Point(27, 16);
            this.imgLength4.Name = "imgLength4";
            this.imgLength4.Size = new System.Drawing.Size(305, 297);
            this.imgLength4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength4.TabIndex = 17;
            this.imgLength4.TabStop = false;
            // 
            // WeightLiters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Proyecto.Properties.Resources.wall16;
            this.ClientSize = new System.Drawing.Size(731, 450);
            this.Controls.Add(this.imgLength6);
            this.Controls.Add(this.imgLength5);
            this.Controls.Add(this.imgLength4);
            this.Controls.Add(this.btnNextWeighConversions);
            this.Controls.Add(this.btnBeforeWeightGrams);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WeightLiters";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Weight Units";
            ((System.ComponentModel.ISupportInitialize)(this.imgLength6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnNextWeighConversions;
        private System.Windows.Forms.Button btnBeforeWeightGrams;
        private System.Windows.Forms.PictureBox imgLength6;
        private System.Windows.Forms.PictureBox imgLength5;
        private System.Windows.Forms.PictureBox imgLength4;
    }
}