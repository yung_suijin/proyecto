﻿using Library;
using Proyecto.LengthMeasures;
using System;
using System.Windows.Forms;

namespace Proyecto
{
    public partial class Menu : Form
    {
        private readonly WindowEvents buttonEvents;

        public Menu()
        {
            InitializeComponent();

            buttonEvents = new WindowEvents();
        }

        private void btnMeasurements_Click(object sender, EventArgs e)
        {
            buttonEvents.CloseCurrentWindowAndOpenNextWindow(this, new Lengths());
        }

        private void btnWeights_Click(object sender, EventArgs e)
        {
            buttonEvents.CloseCurrentWindowAndOpenNextWindow(this, new Weights());
        }
    }
}