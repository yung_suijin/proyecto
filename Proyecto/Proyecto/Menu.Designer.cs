﻿
namespace Proyecto
{
    partial class Menu
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.btnWeights = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnMeasurements = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnWeights
            // 
            this.btnWeights.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnWeights.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnWeights.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnWeights.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnWeights.Location = new System.Drawing.Point(325, 95);
            this.btnWeights.Name = "btnWeights";
            this.btnWeights.Size = new System.Drawing.Size(109, 60);
            this.btnWeights.TabIndex = 1;
            this.btnWeights.Text = "Weight";
            this.btnWeights.UseVisualStyleBackColor = false;
            this.btnWeights.Click += new System.EventHandler(this.btnWeights_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTitle.Font = new System.Drawing.Font("Baloo", 20F);
            this.lblTitle.Location = new System.Drawing.Point(63, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(321, 42);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "Length and Weight Units";
            // 
            // btnMeasurements
            // 
            this.btnMeasurements.BackColor = System.Drawing.Color.PaleGreen;
            this.btnMeasurements.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMeasurements.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMeasurements.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnMeasurements.Location = new System.Drawing.Point(191, 95);
            this.btnMeasurements.Name = "btnMeasurements";
            this.btnMeasurements.Size = new System.Drawing.Size(118, 60);
            this.btnMeasurements.TabIndex = 0;
            this.btnMeasurements.Text = "Length";
            this.btnMeasurements.UseVisualStyleBackColor = false;
            this.btnMeasurements.Click += new System.EventHandler(this.btnMeasurements_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(23)))), ((int)(((byte)(28)))));
            this.BackgroundImage = global::Proyecto.Properties.Resources.wall1;
            this.ClientSize = new System.Drawing.Size(459, 282);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.btnWeights);
            this.Controls.Add(this.btnMeasurements);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnWeights;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Button btnMeasurements;
    }
}

