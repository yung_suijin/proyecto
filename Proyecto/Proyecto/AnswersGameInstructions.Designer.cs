﻿
namespace Proyecto
{
    partial class AnswersGameInstructions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AnswersGameInstructions));
            this.lblTitleLengthExercises = new System.Windows.Forms.Label();
            this.btnCheckGift = new System.Windows.Forms.Button();
            this.txtGiftLength = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitleLengthExercises
            // 
            this.lblTitleLengthExercises.AutoSize = true;
            this.lblTitleLengthExercises.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTitleLengthExercises.Font = new System.Drawing.Font("Baloo", 16F);
            this.lblTitleLengthExercises.Location = new System.Drawing.Point(12, 9);
            this.lblTitleLengthExercises.Name = "lblTitleLengthExercises";
            this.lblTitleLengthExercises.Size = new System.Drawing.Size(467, 105);
            this.lblTitleLengthExercises.TabIndex = 26;
            this.lblTitleLengthExercises.Text = "Write the conversion in the textbox and then \r\nclick the \'check\' button that is o" +
    "n the side of \r\nevery image";
            // 
            // btnCheckGift
            // 
            this.btnCheckGift.BackColor = System.Drawing.Color.LightGreen;
            this.btnCheckGift.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCheckGift.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCheckGift.Font = new System.Drawing.Font("Baloo", 12F);
            this.btnCheckGift.Location = new System.Drawing.Point(241, 225);
            this.btnCheckGift.Name = "btnCheckGift";
            this.btnCheckGift.Size = new System.Drawing.Size(70, 39);
            this.btnCheckGift.TabIndex = 41;
            this.btnCheckGift.Tag = "0.45";
            this.btnCheckGift.Text = "Check";
            this.btnCheckGift.UseVisualStyleBackColor = false;
            // 
            // txtGiftLength
            // 
            this.txtGiftLength.Font = new System.Drawing.Font("Baloo", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiftLength.Location = new System.Drawing.Point(115, 340);
            this.txtGiftLength.Name = "txtGiftLength";
            this.txtGiftLength.Size = new System.Drawing.Size(128, 41);
            this.txtGiftLength.TabIndex = 39;
            this.txtGiftLength.Tag = "";
            this.txtGiftLength.Text = "1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label9.Font = new System.Drawing.Font("Baloo", 16F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(21, 343);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 35);
            this.label9.TabIndex = 40;
            this.label9.Text = "Meters:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label10.Font = new System.Drawing.Font("Baloo", 16F);
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(80, 132);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 35);
            this.label10.TabIndex = 38;
            this.label10.Text = "45 Cm";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox4.Image = global::Proyecto.Properties.Resources.Gift;
            this.pictureBox4.Location = new System.Drawing.Point(45, 170);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(178, 154);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 37;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox1.Image = global::Proyecto.Properties.Resources.RedArrow;
            this.pictureBox1.Location = new System.Drawing.Point(320, 166);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(113, 105);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 44;
            this.pictureBox1.TabStop = false;
            // 
            // AnswersGameInstructions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Proyecto.Properties.Resources.wall12;
            this.ClientSize = new System.Drawing.Size(482, 405);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnCheckGift);
            this.Controls.Add(this.txtGiftLength);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.lblTitleLengthExercises);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AnswersGameInstructions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Game Instructions";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitleLengthExercises;
        private System.Windows.Forms.Button btnCheckGift;
        private System.Windows.Forms.TextBox txtGiftLength;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}