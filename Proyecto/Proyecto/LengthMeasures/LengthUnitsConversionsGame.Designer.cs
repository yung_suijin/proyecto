﻿
namespace Proyecto.LengthMeasures
{
    partial class LengthUnitsConversionsGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LengthUnitsConversionsGame));
            this.label1 = new System.Windows.Forms.Label();
            this.txtTvLength = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMarbleLength = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBeeLength = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDistanceLength = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtGiftLength = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtKidLength = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnCheckTv = new System.Windows.Forms.Button();
            this.btnCheckMarble = new System.Windows.Forms.Button();
            this.btnCheckBee = new System.Windows.Forms.Button();
            this.btnCheckDistance = new System.Windows.Forms.Button();
            this.btnCheckGift = new System.Windows.Forms.Button();
            this.btnCheckKid = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pctGameInstructionsButton = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.imgDog = new System.Windows.Forms.PictureBox();
            this.lblTitleLengthExercises = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctGameInstructionsButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDog)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label1.Font = new System.Drawing.Font("Baloo", 16F);
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(119, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 35);
            this.label1.TabIndex = 8;
            this.label1.Text = "1 M";
            // 
            // txtTvLength
            // 
            this.txtTvLength.Font = new System.Drawing.Font("Baloo", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTvLength.Location = new System.Drawing.Point(157, 331);
            this.txtTvLength.Name = "txtTvLength";
            this.txtTvLength.Size = new System.Drawing.Size(128, 41);
            this.txtTvLength.TabIndex = 9;
            this.txtTvLength.Tag = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Font = new System.Drawing.Font("Baloo", 16F);
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(14, 334);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 35);
            this.label2.TabIndex = 10;
            this.label2.Text = "Centimeters:";
            // 
            // txtMarbleLength
            // 
            this.txtMarbleLength.Font = new System.Drawing.Font("Baloo", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarbleLength.Location = new System.Drawing.Point(430, 328);
            this.txtMarbleLength.Name = "txtMarbleLength";
            this.txtMarbleLength.Size = new System.Drawing.Size(128, 41);
            this.txtMarbleLength.TabIndex = 13;
            this.txtMarbleLength.Tag = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label3.Font = new System.Drawing.Font("Baloo", 16F);
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(298, 334);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 35);
            this.label3.TabIndex = 14;
            this.label3.Text = "Millimeters:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label4.Font = new System.Drawing.Font("Baloo", 16F);
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(393, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 35);
            this.label4.TabIndex = 12;
            this.label4.Text = "2 Cm";
            // 
            // txtBeeLength
            // 
            this.txtBeeLength.Font = new System.Drawing.Font("Baloo", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBeeLength.Location = new System.Drawing.Point(718, 328);
            this.txtBeeLength.Name = "txtBeeLength";
            this.txtBeeLength.Size = new System.Drawing.Size(128, 41);
            this.txtBeeLength.TabIndex = 18;
            this.txtBeeLength.Tag = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label5.Font = new System.Drawing.Font("Baloo", 16F);
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(575, 334);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(146, 35);
            this.label5.TabIndex = 19;
            this.label5.Text = "Centimeters:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label6.Font = new System.Drawing.Font("Baloo", 16F);
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(676, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 35);
            this.label6.TabIndex = 17;
            this.label6.Text = "14 Mm";
            // 
            // txtDistanceLength
            // 
            this.txtDistanceLength.Font = new System.Drawing.Font("Baloo", 16F);
            this.txtDistanceLength.Location = new System.Drawing.Point(107, 600);
            this.txtDistanceLength.Name = "txtDistanceLength";
            this.txtDistanceLength.Size = new System.Drawing.Size(178, 41);
            this.txtDistanceLength.TabIndex = 22;
            this.txtDistanceLength.Tag = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label7.Font = new System.Drawing.Font("Baloo", 16F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(14, 603);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 35);
            this.label7.TabIndex = 23;
            this.label7.Text = "Meters:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label8.Font = new System.Drawing.Font("Baloo", 16F);
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(72, 392);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(132, 35);
            this.label8.TabIndex = 21;
            this.label8.Text = "384,400 Km";
            // 
            // txtGiftLength
            // 
            this.txtGiftLength.Font = new System.Drawing.Font("Baloo", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiftLength.Location = new System.Drawing.Point(427, 600);
            this.txtGiftLength.Name = "txtGiftLength";
            this.txtGiftLength.Size = new System.Drawing.Size(128, 41);
            this.txtGiftLength.TabIndex = 26;
            this.txtGiftLength.Tag = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label9.Font = new System.Drawing.Font("Baloo", 16F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(333, 603);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 35);
            this.label9.TabIndex = 27;
            this.label9.Text = "Meters:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label10.Font = new System.Drawing.Font("Baloo", 16F);
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(392, 392);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 35);
            this.label10.TabIndex = 25;
            this.label10.Text = "45 Cm";
            // 
            // txtKidLength
            // 
            this.txtKidLength.Font = new System.Drawing.Font("Baloo", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKidLength.Location = new System.Drawing.Point(715, 600);
            this.txtKidLength.Name = "txtKidLength";
            this.txtKidLength.Size = new System.Drawing.Size(128, 41);
            this.txtKidLength.TabIndex = 30;
            this.txtKidLength.Tag = "";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label11.Font = new System.Drawing.Font("Baloo", 16F);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(583, 606);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(135, 35);
            this.label11.TabIndex = 31;
            this.label11.Text = "Millimeters:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label12.Font = new System.Drawing.Font("Baloo", 16F);
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(676, 392);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 35);
            this.label12.TabIndex = 29;
            this.label12.Text = "1.2 M";
            // 
            // btnCheckTv
            // 
            this.btnCheckTv.BackColor = System.Drawing.Color.LightGreen;
            this.btnCheckTv.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCheckTv.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCheckTv.Font = new System.Drawing.Font("Baloo", 12F);
            this.btnCheckTv.Location = new System.Drawing.Point(241, 218);
            this.btnCheckTv.Name = "btnCheckTv";
            this.btnCheckTv.Size = new System.Drawing.Size(70, 39);
            this.btnCheckTv.TabIndex = 32;
            this.btnCheckTv.Tag = "100";
            this.btnCheckTv.Text = "Check";
            this.btnCheckTv.UseVisualStyleBackColor = false;
            // 
            // btnCheckMarble
            // 
            this.btnCheckMarble.BackColor = System.Drawing.Color.LightGreen;
            this.btnCheckMarble.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCheckMarble.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCheckMarble.Font = new System.Drawing.Font("Baloo", 12F);
            this.btnCheckMarble.Location = new System.Drawing.Point(524, 218);
            this.btnCheckMarble.Name = "btnCheckMarble";
            this.btnCheckMarble.Size = new System.Drawing.Size(70, 39);
            this.btnCheckMarble.TabIndex = 33;
            this.btnCheckMarble.Tag = "20";
            this.btnCheckMarble.Text = "Check";
            this.btnCheckMarble.UseVisualStyleBackColor = false;
            // 
            // btnCheckBee
            // 
            this.btnCheckBee.BackColor = System.Drawing.Color.LightGreen;
            this.btnCheckBee.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCheckBee.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCheckBee.Font = new System.Drawing.Font("Baloo", 12F);
            this.btnCheckBee.Location = new System.Drawing.Point(807, 218);
            this.btnCheckBee.Name = "btnCheckBee";
            this.btnCheckBee.Size = new System.Drawing.Size(70, 39);
            this.btnCheckBee.TabIndex = 34;
            this.btnCheckBee.Tag = "1.4";
            this.btnCheckBee.Text = "Check";
            this.btnCheckBee.UseVisualStyleBackColor = false;
            // 
            // btnCheckDistance
            // 
            this.btnCheckDistance.BackColor = System.Drawing.Color.LightGreen;
            this.btnCheckDistance.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCheckDistance.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCheckDistance.Font = new System.Drawing.Font("Baloo", 12F);
            this.btnCheckDistance.Location = new System.Drawing.Point(263, 484);
            this.btnCheckDistance.Name = "btnCheckDistance";
            this.btnCheckDistance.Size = new System.Drawing.Size(70, 39);
            this.btnCheckDistance.TabIndex = 35;
            this.btnCheckDistance.Tag = "384400000";
            this.btnCheckDistance.Text = "Check";
            this.btnCheckDistance.UseVisualStyleBackColor = false;
            // 
            // btnCheckGift
            // 
            this.btnCheckGift.BackColor = System.Drawing.Color.LightGreen;
            this.btnCheckGift.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCheckGift.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCheckGift.Font = new System.Drawing.Font("Baloo", 12F);
            this.btnCheckGift.Location = new System.Drawing.Point(541, 484);
            this.btnCheckGift.Name = "btnCheckGift";
            this.btnCheckGift.Size = new System.Drawing.Size(70, 39);
            this.btnCheckGift.TabIndex = 36;
            this.btnCheckGift.Tag = "0.45";
            this.btnCheckGift.Text = "Check";
            this.btnCheckGift.UseVisualStyleBackColor = false;
            // 
            // btnCheckKid
            // 
            this.btnCheckKid.BackColor = System.Drawing.Color.LightGreen;
            this.btnCheckKid.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCheckKid.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCheckKid.Font = new System.Drawing.Font("Baloo", 12F);
            this.btnCheckKid.Location = new System.Drawing.Point(807, 484);
            this.btnCheckKid.Name = "btnCheckKid";
            this.btnCheckKid.Size = new System.Drawing.Size(70, 39);
            this.btnCheckKid.TabIndex = 37;
            this.btnCheckKid.Tag = "1200";
            this.btnCheckKid.Text = "Check";
            this.btnCheckKid.UseVisualStyleBackColor = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox5.Image = global::Proyecto.Properties.Resources.Kid;
            this.pictureBox5.Location = new System.Drawing.Point(623, 430);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(178, 154);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 28;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox4.Image = global::Proyecto.Properties.Resources.Gift;
            this.pictureBox4.Location = new System.Drawing.Point(357, 430);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(178, 154);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 24;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox3.Image = global::Proyecto.Properties.Resources.EarthMoon;
            this.pictureBox3.Location = new System.Drawing.Point(20, 430);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(237, 143);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 20;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox2.Image = global::Proyecto.Properties.Resources.Bee;
            this.pictureBox2.Location = new System.Drawing.Point(623, 164);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(178, 154);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 16;
            this.pictureBox2.TabStop = false;
            // 
            // pctGameInstructionsButton
            // 
            this.pctGameInstructionsButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pctGameInstructionsButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pctGameInstructionsButton.Image = global::Proyecto.Properties.Resources.QuestionButton;
            this.pctGameInstructionsButton.Location = new System.Drawing.Point(754, 12);
            this.pctGameInstructionsButton.Name = "pctGameInstructionsButton";
            this.pctGameInstructionsButton.Size = new System.Drawing.Size(112, 70);
            this.pctGameInstructionsButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pctGameInstructionsButton.TabIndex = 15;
            this.pctGameInstructionsButton.TabStop = false;
            this.pctGameInstructionsButton.Click += new System.EventHandler(this.pctGameInstructionsButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox1.Image = global::Proyecto.Properties.Resources.Marble;
            this.pictureBox1.Location = new System.Drawing.Point(340, 164);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(178, 154);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // imgDog
            // 
            this.imgDog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgDog.Image = global::Proyecto.Properties.Resources.Tv;
            this.imgDog.Location = new System.Drawing.Point(57, 155);
            this.imgDog.Name = "imgDog";
            this.imgDog.Size = new System.Drawing.Size(178, 163);
            this.imgDog.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgDog.TabIndex = 7;
            this.imgDog.TabStop = false;
            // 
            // lblTitleLengthExercises
            // 
            this.lblTitleLengthExercises.AutoSize = true;
            this.lblTitleLengthExercises.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTitleLengthExercises.Font = new System.Drawing.Font("Baloo", 16F);
            this.lblTitleLengthExercises.Location = new System.Drawing.Point(12, 9);
            this.lblTitleLengthExercises.Name = "lblTitleLengthExercises";
            this.lblTitleLengthExercises.Size = new System.Drawing.Size(729, 105);
            this.lblTitleLengthExercises.TabIndex = 4;
            this.lblTitleLengthExercises.Text = "According to the previous explanations about conversions, write in the \r\nwhite bo" +
    "x the actual conversion for the length that belongs to the \r\nobject (red length " +
    "text):";
            // 
            // LengthUnitsConversionsGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Proyecto.Properties.Resources.wall8;
            this.ClientSize = new System.Drawing.Size(898, 670);
            this.Controls.Add(this.btnCheckKid);
            this.Controls.Add(this.btnCheckGift);
            this.Controls.Add(this.btnCheckDistance);
            this.Controls.Add(this.btnCheckBee);
            this.Controls.Add(this.btnCheckMarble);
            this.Controls.Add(this.btnCheckTv);
            this.Controls.Add(this.txtKidLength);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.txtGiftLength);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.txtDistanceLength);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.txtBeeLength);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pctGameInstructionsButton);
            this.Controls.Add(this.txtMarbleLength);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtTvLength);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imgDog);
            this.Controls.Add(this.lblTitleLengthExercises);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LengthUnitsConversionsGame";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Length Units";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctGameInstructionsButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDog)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitleLengthExercises;
        private System.Windows.Forms.PictureBox imgDog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTvLength;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMarbleLength;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pctGameInstructionsButton;
        private System.Windows.Forms.TextBox txtBeeLength;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtDistanceLength;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox txtGiftLength;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TextBox txtKidLength;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Button btnCheckTv;
        private System.Windows.Forms.Button btnCheckMarble;
        private System.Windows.Forms.Button btnCheckBee;
        private System.Windows.Forms.Button btnCheckDistance;
        private System.Windows.Forms.Button btnCheckGift;
        private System.Windows.Forms.Button btnCheckKid;
    }
}