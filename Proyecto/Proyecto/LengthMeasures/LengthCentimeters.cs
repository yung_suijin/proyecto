﻿using Library;
using System;
using System.Windows.Forms;

namespace Proyecto.LengthMeasures
{
    public partial class LengthCentimeters : Form
    {
        private readonly WindowEvents windowEvents;

        public LengthCentimeters()
        {
            InitializeComponent();

            windowEvents = new WindowEvents();
        }

        private void btnBeforeLengthMeters_Click(object sender, EventArgs e)
        {
            windowEvents.CloseCurrentWindowAndOpenNextWindow(this, new LengthMeters());
        }

        private void btnNextLengthKilometers_Click(object sender, EventArgs e)
        {
            windowEvents.CloseCurrentWindowAndOpenNextWindow(this, new LengthKilometers());
        }
    }
}