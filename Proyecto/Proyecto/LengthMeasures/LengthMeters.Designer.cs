﻿
namespace Proyecto.LengthMeasures
{
    partial class LengthMeters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LengthMeters));
            this.imgLength4 = new System.Windows.Forms.PictureBox();
            this.imgLength5 = new System.Windows.Forms.PictureBox();
            this.imgLength6 = new System.Windows.Forms.PictureBox();
            this.btnNextLengthCentimeters = new System.Windows.Forms.Button();
            this.btnBeforeLengths = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength6)).BeginInit();
            this.SuspendLayout();
            // 
            // imgLength4
            // 
            this.imgLength4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength4.Image = global::Proyecto.Properties.Resources.MeterInfo;
            this.imgLength4.Location = new System.Drawing.Point(24, 12);
            this.imgLength4.Name = "imgLength4";
            this.imgLength4.Size = new System.Drawing.Size(305, 297);
            this.imgLength4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength4.TabIndex = 1;
            this.imgLength4.TabStop = false;
            // 
            // imgLength5
            // 
            this.imgLength5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength5.Image = global::Proyecto.Properties.Resources.MeterRuler;
            this.imgLength5.Location = new System.Drawing.Point(355, -1);
            this.imgLength5.Name = "imgLength5";
            this.imgLength5.Size = new System.Drawing.Size(351, 323);
            this.imgLength5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength5.TabIndex = 2;
            this.imgLength5.TabStop = false;
            // 
            // imgLength6
            // 
            this.imgLength6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength6.Image = global::Proyecto.Properties.Resources.MeterParts;
            this.imgLength6.Location = new System.Drawing.Point(24, 328);
            this.imgLength6.Name = "imgLength6";
            this.imgLength6.Size = new System.Drawing.Size(428, 111);
            this.imgLength6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength6.TabIndex = 6;
            this.imgLength6.TabStop = false;
            // 
            // btnNextLengthCentimeters
            // 
            this.btnNextLengthCentimeters.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnNextLengthCentimeters.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNextLengthCentimeters.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNextLengthCentimeters.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnNextLengthCentimeters.Location = new System.Drawing.Point(597, 355);
            this.btnNextLengthCentimeters.Name = "btnNextLengthCentimeters";
            this.btnNextLengthCentimeters.Size = new System.Drawing.Size(109, 60);
            this.btnNextLengthCentimeters.TabIndex = 8;
            this.btnNextLengthCentimeters.Text = "Next";
            this.btnNextLengthCentimeters.UseVisualStyleBackColor = false;
            this.btnNextLengthCentimeters.Click += new System.EventHandler(this.btnNextLengthCentimeters_Click);
            // 
            // btnBeforeLengths
            // 
            this.btnBeforeLengths.BackColor = System.Drawing.Color.LightGreen;
            this.btnBeforeLengths.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBeforeLengths.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBeforeLengths.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnBeforeLengths.Location = new System.Drawing.Point(471, 355);
            this.btnBeforeLengths.Name = "btnBeforeLengths";
            this.btnBeforeLengths.Size = new System.Drawing.Size(109, 60);
            this.btnBeforeLengths.TabIndex = 7;
            this.btnBeforeLengths.Text = "Before";
            this.btnBeforeLengths.UseVisualStyleBackColor = false;
            this.btnBeforeLengths.Click += new System.EventHandler(this.btnBeforeLengths_Click);
            // 
            // LengthMeters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Proyecto.Properties.Resources.wall4;
            this.ClientSize = new System.Drawing.Size(731, 450);
            this.Controls.Add(this.btnNextLengthCentimeters);
            this.Controls.Add(this.btnBeforeLengths);
            this.Controls.Add(this.imgLength6);
            this.Controls.Add(this.imgLength5);
            this.Controls.Add(this.imgLength4);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LengthMeters";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Length Units";
            ((System.ComponentModel.ISupportInitialize)(this.imgLength4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox imgLength4;
        private System.Windows.Forms.PictureBox imgLength5;
        private System.Windows.Forms.PictureBox imgLength6;
        private System.Windows.Forms.Button btnNextLengthCentimeters;
        private System.Windows.Forms.Button btnBeforeLengths;
    }
}