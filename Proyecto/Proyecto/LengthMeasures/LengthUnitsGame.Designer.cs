﻿
namespace Proyecto.LengthMeasures
{
    partial class LengthUnitsGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LengthUnitsGame));
            this.lblTitleLengthExercises = new System.Windows.Forms.Label();
            this.AnswerBoxDog = new System.Windows.Forms.PictureBox();
            this.imgEarth = new System.Windows.Forms.PictureBox();
            this.imgHuman = new System.Windows.Forms.PictureBox();
            this.imgAnt = new System.Windows.Forms.PictureBox();
            this.imgPlant = new System.Windows.Forms.PictureBox();
            this.imgMillimeter = new System.Windows.Forms.PictureBox();
            this.imgCentimeter = new System.Windows.Forms.PictureBox();
            this.imgKilometer = new System.Windows.Forms.PictureBox();
            this.imgMeter = new System.Windows.Forms.PictureBox();
            this.imgMap = new System.Windows.Forms.PictureBox();
            this.imgPlane = new System.Windows.Forms.PictureBox();
            this.imgDog = new System.Windows.Forms.PictureBox();
            this.AnswerBoxMap = new System.Windows.Forms.PictureBox();
            this.AnswerBoxEarth = new System.Windows.Forms.PictureBox();
            this.AnswerBoxAnt = new System.Windows.Forms.PictureBox();
            this.AnswerBoxHuman = new System.Windows.Forms.PictureBox();
            this.AnswerBoxPlant = new System.Windows.Forms.PictureBox();
            this.AnswerBoxPlane = new System.Windows.Forms.PictureBox();
            this.pctGameInstructionsButton = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxDog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEarth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgHuman)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAnt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMillimeter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCentimeter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgKilometer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMeter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPlane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxEarth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxAnt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxHuman)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxPlane)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctGameInstructionsButton)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitleLengthExercises
            // 
            this.lblTitleLengthExercises.AutoSize = true;
            this.lblTitleLengthExercises.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTitleLengthExercises.Font = new System.Drawing.Font("Baloo", 16F);
            this.lblTitleLengthExercises.Location = new System.Drawing.Point(12, 9);
            this.lblTitleLengthExercises.Name = "lblTitleLengthExercises";
            this.lblTitleLengthExercises.Size = new System.Drawing.Size(720, 70);
            this.lblTitleLengthExercises.TabIndex = 3;
            this.lblTitleLengthExercises.Text = "According to the previous explanations about lengths, drag the length \r\ntype imag" +
    "e that you think it can measure length for different things:";
            // 
            // AnswerBoxDog
            // 
            this.AnswerBoxDog.BackColor = System.Drawing.Color.White;
            this.AnswerBoxDog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AnswerBoxDog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnswerBoxDog.Location = new System.Drawing.Point(20, 281);
            this.AnswerBoxDog.Name = "AnswerBoxDog";
            this.AnswerBoxDog.Size = new System.Drawing.Size(148, 39);
            this.AnswerBoxDog.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AnswerBoxDog.TabIndex = 17;
            this.AnswerBoxDog.TabStop = false;
            this.AnswerBoxDog.Tag = "Centimeter";
            // 
            // imgEarth
            // 
            this.imgEarth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgEarth.Image = global::Proyecto.Properties.Resources.Earth;
            this.imgEarth.Location = new System.Drawing.Point(26, 349);
            this.imgEarth.Name = "imgEarth";
            this.imgEarth.Size = new System.Drawing.Size(168, 160);
            this.imgEarth.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgEarth.TabIndex = 16;
            this.imgEarth.TabStop = false;
            // 
            // imgHuman
            // 
            this.imgHuman.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgHuman.Image = global::Proyecto.Properties.Resources.Human;
            this.imgHuman.Location = new System.Drawing.Point(446, 266);
            this.imgHuman.Name = "imgHuman";
            this.imgHuman.Size = new System.Drawing.Size(116, 234);
            this.imgHuman.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgHuman.TabIndex = 15;
            this.imgHuman.TabStop = false;
            // 
            // imgAnt
            // 
            this.imgAnt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgAnt.Image = global::Proyecto.Properties.Resources.Ant;
            this.imgAnt.Location = new System.Drawing.Point(221, 334);
            this.imgAnt.Name = "imgAnt";
            this.imgAnt.Size = new System.Drawing.Size(199, 121);
            this.imgAnt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgAnt.TabIndex = 14;
            this.imgAnt.TabStop = false;
            // 
            // imgPlant
            // 
            this.imgPlant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgPlant.Image = global::Proyecto.Properties.Resources.Plant;
            this.imgPlant.Location = new System.Drawing.Point(629, 349);
            this.imgPlant.Name = "imgPlant";
            this.imgPlant.Size = new System.Drawing.Size(107, 165);
            this.imgPlant.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgPlant.TabIndex = 13;
            this.imgPlant.TabStop = false;
            // 
            // imgMillimeter
            // 
            this.imgMillimeter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgMillimeter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgMillimeter.Location = new System.Drawing.Point(558, 602);
            this.imgMillimeter.Name = "imgMillimeter";
            this.imgMillimeter.Size = new System.Drawing.Size(124, 33);
            this.imgMillimeter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgMillimeter.TabIndex = 12;
            this.imgMillimeter.TabStop = false;
            this.imgMillimeter.Tag = "Millimeter";
            // 
            // imgCentimeter
            // 
            this.imgCentimeter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgCentimeter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgCentimeter.Location = new System.Drawing.Point(395, 602);
            this.imgCentimeter.Name = "imgCentimeter";
            this.imgCentimeter.Size = new System.Drawing.Size(138, 33);
            this.imgCentimeter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgCentimeter.TabIndex = 11;
            this.imgCentimeter.TabStop = false;
            this.imgCentimeter.Tag = "Centimeter";
            // 
            // imgKilometer
            // 
            this.imgKilometer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgKilometer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgKilometer.Location = new System.Drawing.Point(239, 602);
            this.imgKilometer.Name = "imgKilometer";
            this.imgKilometer.Size = new System.Drawing.Size(124, 33);
            this.imgKilometer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgKilometer.TabIndex = 10;
            this.imgKilometer.TabStop = false;
            this.imgKilometer.Tag = "Kilometer";
            // 
            // imgMeter
            // 
            this.imgMeter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgMeter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgMeter.Location = new System.Drawing.Point(115, 602);
            this.imgMeter.Name = "imgMeter";
            this.imgMeter.Size = new System.Drawing.Size(88, 33);
            this.imgMeter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgMeter.TabIndex = 9;
            this.imgMeter.TabStop = false;
            this.imgMeter.Tag = "Meter";
            // 
            // imgMap
            // 
            this.imgMap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgMap.Image = global::Proyecto.Properties.Resources.Map;
            this.imgMap.Location = new System.Drawing.Point(516, 95);
            this.imgMap.Name = "imgMap";
            this.imgMap.Size = new System.Drawing.Size(268, 165);
            this.imgMap.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgMap.TabIndex = 8;
            this.imgMap.TabStop = false;
            // 
            // imgPlane
            // 
            this.imgPlane.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgPlane.Image = ((System.Drawing.Image)(resources.GetObject("imgPlane.Image")));
            this.imgPlane.Location = new System.Drawing.Point(169, 119);
            this.imgPlane.Name = "imgPlane";
            this.imgPlane.Size = new System.Drawing.Size(336, 119);
            this.imgPlane.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgPlane.TabIndex = 7;
            this.imgPlane.TabStop = false;
            // 
            // imgDog
            // 
            this.imgDog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgDog.Image = global::Proyecto.Properties.Resources.Dog;
            this.imgDog.Location = new System.Drawing.Point(26, 95);
            this.imgDog.Name = "imgDog";
            this.imgDog.Size = new System.Drawing.Size(129, 165);
            this.imgDog.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgDog.TabIndex = 6;
            this.imgDog.TabStop = false;
            // 
            // AnswerBoxMap
            // 
            this.AnswerBoxMap.BackColor = System.Drawing.Color.White;
            this.AnswerBoxMap.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AnswerBoxMap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnswerBoxMap.Location = new System.Drawing.Point(599, 270);
            this.AnswerBoxMap.Name = "AnswerBoxMap";
            this.AnswerBoxMap.Size = new System.Drawing.Size(148, 39);
            this.AnswerBoxMap.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AnswerBoxMap.TabIndex = 19;
            this.AnswerBoxMap.TabStop = false;
            this.AnswerBoxMap.Tag = "Kilometer";
            // 
            // AnswerBoxEarth
            // 
            this.AnswerBoxEarth.BackColor = System.Drawing.Color.White;
            this.AnswerBoxEarth.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AnswerBoxEarth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnswerBoxEarth.Location = new System.Drawing.Point(35, 524);
            this.AnswerBoxEarth.Name = "AnswerBoxEarth";
            this.AnswerBoxEarth.Size = new System.Drawing.Size(148, 39);
            this.AnswerBoxEarth.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AnswerBoxEarth.TabIndex = 20;
            this.AnswerBoxEarth.TabStop = false;
            this.AnswerBoxEarth.Tag = "Kilometer";
            // 
            // AnswerBoxAnt
            // 
            this.AnswerBoxAnt.BackColor = System.Drawing.Color.White;
            this.AnswerBoxAnt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AnswerBoxAnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnswerBoxAnt.Location = new System.Drawing.Point(239, 470);
            this.AnswerBoxAnt.Name = "AnswerBoxAnt";
            this.AnswerBoxAnt.Size = new System.Drawing.Size(148, 39);
            this.AnswerBoxAnt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AnswerBoxAnt.TabIndex = 21;
            this.AnswerBoxAnt.TabStop = false;
            this.AnswerBoxAnt.Tag = "Millimeter";
            // 
            // AnswerBoxHuman
            // 
            this.AnswerBoxHuman.BackColor = System.Drawing.Color.White;
            this.AnswerBoxHuman.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AnswerBoxHuman.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnswerBoxHuman.Location = new System.Drawing.Point(424, 512);
            this.AnswerBoxHuman.Name = "AnswerBoxHuman";
            this.AnswerBoxHuman.Size = new System.Drawing.Size(148, 39);
            this.AnswerBoxHuman.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AnswerBoxHuman.TabIndex = 22;
            this.AnswerBoxHuman.TabStop = false;
            this.AnswerBoxHuman.Tag = "Meter";
            // 
            // AnswerBoxPlant
            // 
            this.AnswerBoxPlant.BackColor = System.Drawing.Color.White;
            this.AnswerBoxPlant.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AnswerBoxPlant.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnswerBoxPlant.Location = new System.Drawing.Point(608, 524);
            this.AnswerBoxPlant.Name = "AnswerBoxPlant";
            this.AnswerBoxPlant.Size = new System.Drawing.Size(148, 39);
            this.AnswerBoxPlant.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AnswerBoxPlant.TabIndex = 23;
            this.AnswerBoxPlant.TabStop = false;
            this.AnswerBoxPlant.Tag = "Centimeter";
            // 
            // AnswerBoxPlane
            // 
            this.AnswerBoxPlane.BackColor = System.Drawing.Color.White;
            this.AnswerBoxPlane.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AnswerBoxPlane.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnswerBoxPlane.Location = new System.Drawing.Point(251, 255);
            this.AnswerBoxPlane.Name = "AnswerBoxPlane";
            this.AnswerBoxPlane.Size = new System.Drawing.Size(148, 39);
            this.AnswerBoxPlane.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AnswerBoxPlane.TabIndex = 18;
            this.AnswerBoxPlane.TabStop = false;
            this.AnswerBoxPlane.Tag = "Meter";
            // 
            // pctGameInstructionsButton
            // 
            this.pctGameInstructionsButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pctGameInstructionsButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pctGameInstructionsButton.Image = global::Proyecto.Properties.Resources.QuestionButton;
            this.pctGameInstructionsButton.Location = new System.Drawing.Point(728, 9);
            this.pctGameInstructionsButton.Name = "pctGameInstructionsButton";
            this.pctGameInstructionsButton.Size = new System.Drawing.Size(81, 60);
            this.pctGameInstructionsButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pctGameInstructionsButton.TabIndex = 24;
            this.pctGameInstructionsButton.TabStop = false;
            this.pctGameInstructionsButton.Click += new System.EventHandler(this.pctGameInstructionsButton_Click);
            // 
            // LengthUnitsGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Proyecto.Properties.Resources.wall7;
            this.ClientSize = new System.Drawing.Size(816, 656);
            this.Controls.Add(this.pctGameInstructionsButton);
            this.Controls.Add(this.AnswerBoxPlant);
            this.Controls.Add(this.AnswerBoxHuman);
            this.Controls.Add(this.AnswerBoxAnt);
            this.Controls.Add(this.AnswerBoxEarth);
            this.Controls.Add(this.AnswerBoxMap);
            this.Controls.Add(this.AnswerBoxPlane);
            this.Controls.Add(this.AnswerBoxDog);
            this.Controls.Add(this.imgEarth);
            this.Controls.Add(this.imgHuman);
            this.Controls.Add(this.imgAnt);
            this.Controls.Add(this.imgPlant);
            this.Controls.Add(this.imgMillimeter);
            this.Controls.Add(this.imgCentimeter);
            this.Controls.Add(this.imgKilometer);
            this.Controls.Add(this.imgMeter);
            this.Controls.Add(this.imgMap);
            this.Controls.Add(this.imgPlane);
            this.Controls.Add(this.imgDog);
            this.Controls.Add(this.lblTitleLengthExercises);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LengthUnitsGame";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Length Units";
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxDog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEarth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgHuman)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAnt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMillimeter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCentimeter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgKilometer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMeter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPlane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxEarth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxAnt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxHuman)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxPlane)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctGameInstructionsButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitleLengthExercises;
        private System.Windows.Forms.PictureBox imgDog;
        private System.Windows.Forms.PictureBox imgPlane;
        private System.Windows.Forms.PictureBox imgMap;
        private System.Windows.Forms.PictureBox imgKilometer;
        private System.Windows.Forms.PictureBox imgCentimeter;
        private System.Windows.Forms.PictureBox imgMillimeter;
        private System.Windows.Forms.PictureBox imgPlant;
        private System.Windows.Forms.PictureBox imgAnt;
        private System.Windows.Forms.PictureBox imgHuman;
        private System.Windows.Forms.PictureBox imgEarth;
        private System.Windows.Forms.PictureBox imgMeter;
        private System.Windows.Forms.PictureBox AnswerBoxDog;
        private System.Windows.Forms.PictureBox AnswerBoxMap;
        private System.Windows.Forms.PictureBox AnswerBoxEarth;
        private System.Windows.Forms.PictureBox AnswerBoxAnt;
        private System.Windows.Forms.PictureBox AnswerBoxHuman;
        private System.Windows.Forms.PictureBox AnswerBoxPlant;
        private System.Windows.Forms.PictureBox AnswerBoxPlane;
        private System.Windows.Forms.PictureBox pctGameInstructionsButton;
    }
}