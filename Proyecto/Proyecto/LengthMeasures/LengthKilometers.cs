﻿using Library;
using System;
using System.Windows.Forms;

namespace Proyecto.LengthMeasures
{
    public partial class LengthKilometers : Form
    {
        private readonly WindowEvents windowEvents;

        public LengthKilometers()
        {
            InitializeComponent();

            windowEvents = new WindowEvents();
        }

        private void btnBeforeLengthCentimeters_Click(object sender, EventArgs e)
        {
            windowEvents.CloseCurrentWindowAndOpenNextWindow(this, new LengthCentimeters());
        }

        private void btnNextLengthUnitsConversions_Click(object sender, EventArgs e)
        {
            windowEvents.CloseCurrentWindowAndOpenNextWindow(this, new LengthUnitsConversions());
        }
    }
}