﻿
namespace Proyecto.LengthMeasures
{
    partial class Lengths
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Lengths));
            this.imgLength1 = new System.Windows.Forms.PictureBox();
            this.imgLength2 = new System.Windows.Forms.PictureBox();
            this.btnMenu = new System.Windows.Forms.Button();
            this.btnNextLengthMeters = new System.Windows.Forms.Button();
            this.imgLength3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength3)).BeginInit();
            this.SuspendLayout();
            // 
            // imgLength1
            // 
            this.imgLength1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength1.Image = global::Proyecto.Properties.Resources.LengthMeasure;
            this.imgLength1.Location = new System.Drawing.Point(42, -1);
            this.imgLength1.Name = "imgLength1";
            this.imgLength1.Size = new System.Drawing.Size(325, 310);
            this.imgLength1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength1.TabIndex = 0;
            this.imgLength1.TabStop = false;
            // 
            // imgLength2
            // 
            this.imgLength2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength2.Image = global::Proyecto.Properties.Resources.LengthTypes;
            this.imgLength2.Location = new System.Drawing.Point(391, 42);
            this.imgLength2.Name = "imgLength2";
            this.imgLength2.Size = new System.Drawing.Size(305, 297);
            this.imgLength2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength2.TabIndex = 1;
            this.imgLength2.TabStop = false;
            // 
            // btnMenu
            // 
            this.btnMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnMenu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMenu.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMenu.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnMenu.Location = new System.Drawing.Point(461, 365);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(109, 60);
            this.btnMenu.TabIndex = 3;
            this.btnMenu.Text = "Menu";
            this.btnMenu.UseVisualStyleBackColor = false;
            this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnNextLengthMeters
            // 
            this.btnNextLengthMeters.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnNextLengthMeters.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNextLengthMeters.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNextLengthMeters.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnNextLengthMeters.Location = new System.Drawing.Point(587, 365);
            this.btnNextLengthMeters.Name = "btnNextLengthMeters";
            this.btnNextLengthMeters.Size = new System.Drawing.Size(109, 60);
            this.btnNextLengthMeters.TabIndex = 4;
            this.btnNextLengthMeters.Text = "Next";
            this.btnNextLengthMeters.UseVisualStyleBackColor = false;
            this.btnNextLengthMeters.Click += new System.EventHandler(this.btnNextLengthMeters_Click);
            // 
            // imgLength3
            // 
            this.imgLength3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength3.Image = global::Proyecto.Properties.Resources.LengthUnits;
            this.imgLength3.Location = new System.Drawing.Point(29, 329);
            this.imgLength3.Name = "imgLength3";
            this.imgLength3.Size = new System.Drawing.Size(356, 96);
            this.imgLength3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength3.TabIndex = 5;
            this.imgLength3.TabStop = false;
            // 
            // Lengths
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Proyecto.Properties.Resources.wall2;
            this.ClientSize = new System.Drawing.Size(732, 450);
            this.Controls.Add(this.imgLength3);
            this.Controls.Add(this.btnNextLengthMeters);
            this.Controls.Add(this.btnMenu);
            this.Controls.Add(this.imgLength2);
            this.Controls.Add(this.imgLength1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Lengths";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Length Units";
            ((System.ComponentModel.ISupportInitialize)(this.imgLength1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox imgLength1;
        private System.Windows.Forms.PictureBox imgLength2;
        private System.Windows.Forms.Button btnMenu;
        private System.Windows.Forms.Button btnNextLengthMeters;
        private System.Windows.Forms.PictureBox imgLength3;
    }
}