﻿
namespace Proyecto.LengthMeasures
{
    partial class LengthUnitsConversions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LengthUnitsConversions));
            this.lblTitleConversions = new System.Windows.Forms.Label();
            this.imgLength13 = new System.Windows.Forms.PictureBox();
            this.imgLength14 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.imgLength15 = new System.Windows.Forms.PictureBox();
            this.btnNextLengthConversions = new System.Windows.Forms.Button();
            this.btnBeforeKilometers = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength15)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitleConversions
            // 
            this.lblTitleConversions.AutoSize = true;
            this.lblTitleConversions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTitleConversions.Font = new System.Drawing.Font("Baloo", 20F);
            this.lblTitleConversions.Location = new System.Drawing.Point(170, 9);
            this.lblTitleConversions.Name = "lblTitleConversions";
            this.lblTitleConversions.Size = new System.Drawing.Size(172, 42);
            this.lblTitleConversions.TabIndex = 3;
            this.lblTitleConversions.Text = "Conversions";
            // 
            // imgLength13
            // 
            this.imgLength13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength13.Image = global::Proyecto.Properties.Resources.LengthSmallerParts;
            this.imgLength13.Location = new System.Drawing.Point(21, 54);
            this.imgLength13.Name = "imgLength13";
            this.imgLength13.Size = new System.Drawing.Size(473, 139);
            this.imgLength13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength13.TabIndex = 4;
            this.imgLength13.TabStop = false;
            // 
            // imgLength14
            // 
            this.imgLength14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength14.Image = global::Proyecto.Properties.Resources.LengthConversions;
            this.imgLength14.Location = new System.Drawing.Point(519, 12);
            this.imgLength14.Name = "imgLength14";
            this.imgLength14.Size = new System.Drawing.Size(259, 384);
            this.imgLength14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength14.TabIndex = 5;
            this.imgLength14.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label1.Font = new System.Drawing.Font("Baloo", 20F);
            this.label1.Location = new System.Drawing.Point(168, 216);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 42);
            this.label1.TabIndex = 6;
            this.label1.Text = "Examples:";
            // 
            // imgLength15
            // 
            this.imgLength15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength15.Image = global::Proyecto.Properties.Resources.LengthUnitsConversions;
            this.imgLength15.Location = new System.Drawing.Point(12, 262);
            this.imgLength15.Name = "imgLength15";
            this.imgLength15.Size = new System.Drawing.Size(492, 188);
            this.imgLength15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength15.TabIndex = 7;
            this.imgLength15.TabStop = false;
            // 
            // btnNextLengthConversions
            // 
            this.btnNextLengthConversions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnNextLengthConversions.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNextLengthConversions.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNextLengthConversions.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnNextLengthConversions.Location = new System.Drawing.Point(659, 415);
            this.btnNextLengthConversions.Name = "btnNextLengthConversions";
            this.btnNextLengthConversions.Size = new System.Drawing.Size(109, 60);
            this.btnNextLengthConversions.TabIndex = 10;
            this.btnNextLengthConversions.Text = "Next";
            this.btnNextLengthConversions.UseVisualStyleBackColor = false;
            this.btnNextLengthConversions.Click += new System.EventHandler(this.btnNextLengthConversions_Click);
            // 
            // btnBeforeKilometers
            // 
            this.btnBeforeKilometers.BackColor = System.Drawing.Color.LightGreen;
            this.btnBeforeKilometers.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBeforeKilometers.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBeforeKilometers.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnBeforeKilometers.Location = new System.Drawing.Point(533, 415);
            this.btnBeforeKilometers.Name = "btnBeforeKilometers";
            this.btnBeforeKilometers.Size = new System.Drawing.Size(109, 60);
            this.btnBeforeKilometers.TabIndex = 9;
            this.btnBeforeKilometers.Text = "Before";
            this.btnBeforeKilometers.UseVisualStyleBackColor = false;
            this.btnBeforeKilometers.Click += new System.EventHandler(this.btnBeforeKilometers_Click);
            // 
            // LengthUnitsConversions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Proyecto.Properties.Resources.wall6;
            this.ClientSize = new System.Drawing.Size(800, 496);
            this.Controls.Add(this.btnNextLengthConversions);
            this.Controls.Add(this.btnBeforeKilometers);
            this.Controls.Add(this.imgLength15);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imgLength14);
            this.Controls.Add(this.imgLength13);
            this.Controls.Add(this.lblTitleConversions);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LengthUnitsConversions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Length Units";
            ((System.ComponentModel.ISupportInitialize)(this.imgLength13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength15)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitleConversions;
        private System.Windows.Forms.PictureBox imgLength13;
        private System.Windows.Forms.PictureBox imgLength14;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox imgLength15;
        private System.Windows.Forms.Button btnNextLengthConversions;
        private System.Windows.Forms.Button btnBeforeKilometers;
    }
}