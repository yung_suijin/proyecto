﻿
namespace Proyecto.LengthMeasures
{
    partial class LengthKilometers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LengthKilometers));
            this.imgLength10 = new System.Windows.Forms.PictureBox();
            this.imgLength12 = new System.Windows.Forms.PictureBox();
            this.imgLength11 = new System.Windows.Forms.PictureBox();
            this.btnNextLengthUnitsConversions = new System.Windows.Forms.Button();
            this.btnBeforeLengthCentimeters = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength11)).BeginInit();
            this.SuspendLayout();
            // 
            // imgLength10
            // 
            this.imgLength10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength10.Image = global::Proyecto.Properties.Resources.KilometerInfo;
            this.imgLength10.Location = new System.Drawing.Point(24, 12);
            this.imgLength10.Name = "imgLength10";
            this.imgLength10.Size = new System.Drawing.Size(305, 297);
            this.imgLength10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength10.TabIndex = 3;
            this.imgLength10.TabStop = false;
            // 
            // imgLength12
            // 
            this.imgLength12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength12.Image = global::Proyecto.Properties.Resources.KilometersParts;
            this.imgLength12.Location = new System.Drawing.Point(-5, 328);
            this.imgLength12.Name = "imgLength12";
            this.imgLength12.Size = new System.Drawing.Size(436, 110);
            this.imgLength12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength12.TabIndex = 12;
            this.imgLength12.TabStop = false;
            // 
            // imgLength11
            // 
            this.imgLength11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength11.Image = global::Proyecto.Properties.Resources.Kilometers;
            this.imgLength11.Location = new System.Drawing.Point(355, -1);
            this.imgLength11.Name = "imgLength11";
            this.imgLength11.Size = new System.Drawing.Size(351, 323);
            this.imgLength11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength11.TabIndex = 13;
            this.imgLength11.TabStop = false;
            // 
            // btnNextLengthUnitsConversions
            // 
            this.btnNextLengthUnitsConversions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnNextLengthUnitsConversions.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNextLengthUnitsConversions.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNextLengthUnitsConversions.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnNextLengthUnitsConversions.Location = new System.Drawing.Point(597, 355);
            this.btnNextLengthUnitsConversions.Name = "btnNextLengthUnitsConversions";
            this.btnNextLengthUnitsConversions.Size = new System.Drawing.Size(109, 60);
            this.btnNextLengthUnitsConversions.TabIndex = 15;
            this.btnNextLengthUnitsConversions.Text = "Next";
            this.btnNextLengthUnitsConversions.UseVisualStyleBackColor = false;
            this.btnNextLengthUnitsConversions.Click += new System.EventHandler(this.btnNextLengthUnitsConversions_Click);
            // 
            // btnBeforeLengthCentimeters
            // 
            this.btnBeforeLengthCentimeters.BackColor = System.Drawing.Color.LightGreen;
            this.btnBeforeLengthCentimeters.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBeforeLengthCentimeters.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBeforeLengthCentimeters.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnBeforeLengthCentimeters.Location = new System.Drawing.Point(471, 355);
            this.btnBeforeLengthCentimeters.Name = "btnBeforeLengthCentimeters";
            this.btnBeforeLengthCentimeters.Size = new System.Drawing.Size(109, 60);
            this.btnBeforeLengthCentimeters.TabIndex = 14;
            this.btnBeforeLengthCentimeters.Text = "Before";
            this.btnBeforeLengthCentimeters.UseVisualStyleBackColor = false;
            this.btnBeforeLengthCentimeters.Click += new System.EventHandler(this.btnBeforeLengthCentimeters_Click);
            // 
            // LengthKilometers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Proyecto.Properties.Resources.wall3;
            this.ClientSize = new System.Drawing.Size(731, 450);
            this.Controls.Add(this.btnNextLengthUnitsConversions);
            this.Controls.Add(this.btnBeforeLengthCentimeters);
            this.Controls.Add(this.imgLength11);
            this.Controls.Add(this.imgLength12);
            this.Controls.Add(this.imgLength10);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LengthKilometers";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Length Units";
            ((System.ComponentModel.ISupportInitialize)(this.imgLength10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength11)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox imgLength10;
        private System.Windows.Forms.PictureBox imgLength12;
        private System.Windows.Forms.PictureBox imgLength11;
        private System.Windows.Forms.Button btnNextLengthUnitsConversions;
        private System.Windows.Forms.Button btnBeforeLengthCentimeters;
    }
}