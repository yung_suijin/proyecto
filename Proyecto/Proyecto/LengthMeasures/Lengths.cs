﻿using Library;
using System;
using System.Windows.Forms;

namespace Proyecto.LengthMeasures
{
    public partial class Lengths : Form
    {
        private readonly WindowEvents windowEvents;

        public Lengths()
        {
            InitializeComponent();

            windowEvents = new WindowEvents();
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            windowEvents.CloseCurrentWindowAndOpenNextWindow(this, new Menu());
        }

        private void btnNextLengthMeters_Click(object sender, EventArgs e)
        {
            windowEvents.CloseCurrentWindowAndOpenNextWindow(this, new LengthMeters());
        }
    }
}