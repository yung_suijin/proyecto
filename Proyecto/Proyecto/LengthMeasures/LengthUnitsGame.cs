﻿using Library;
using System.Windows.Forms;

namespace Proyecto.LengthMeasures
{
    public partial class LengthUnitsGame : Form
    {
        private readonly WindowEvents windowEvents;
        private readonly DragAndDropEvents dragAndDropEvents;
        private LengthUnitsConversionsGame lengthUnitsConversionsGame;
        private PictureBox[] images;
        private PictureBox selected;

        public LengthUnitsGame()
        {
            InitializeComponent();

            windowEvents = new WindowEvents();
            dragAndDropEvents = new DragAndDropEvents();
            lengthUnitsConversionsGame = new LengthUnitsConversionsGame();

            images = new PictureBox[] { AnswerBoxDog, AnswerBoxPlane, AnswerBoxMap, AnswerBoxEarth, AnswerBoxAnt,
                AnswerBoxHuman, AnswerBoxPlant, imgMeter, imgMillimeter, imgCentimeter, imgKilometer };

            imgMeter.BackgroundImage = Proyecto.Properties.Resources.Meter;
            imgKilometer.BackgroundImage = Proyecto.Properties.Resources.Kilometer;
            imgCentimeter.BackgroundImage = Proyecto.Properties.Resources.Centimeter;
            imgMillimeter.BackgroundImage = Proyecto.Properties.Resources.Millimeter;

            foreach (var image in images)
            {
                image.AllowDrop = true;
                image.DragDrop += DragDropImage;
                image.DragEnter += dragAndDropEvents.DragEnterImage;
                image.MouseClick += ImageMouseClick;
                image.MouseMove += dragAndDropEvents.ImageMouseMove;
            }
        }

        private void DragDropImage(object sender, DragEventArgs e)
        {
            var answerBox = (PictureBox)sender;

            if (e.Data.GetDataPresent(typeof(PictureBox)))
            {
                var imageLengthUnit = (PictureBox)e.Data.GetData(typeof(PictureBox));

                if (imageLengthUnit != answerBox)
                {
                    dragAndDropEvents.DropAndCheck(imageLengthUnit, answerBox, this, lengthUnitsConversionsGame);

                    selected = null;
                    dragAndDropEvents.SelectedImage(answerBox, images, selected);

                    return;
                }
            }
        }

        private void ImageMouseClick(object sender, MouseEventArgs e)
        {
            dragAndDropEvents.SelectedImage((PictureBox)sender, images, selected);
        }

        private void pctGameInstructionsButton_Click(object sender, System.EventArgs e)
        {
            windowEvents.MaintainCurrentWindowOpenAndOpenNextWindow(new DragDropGameInstructions());
        }
    }
}