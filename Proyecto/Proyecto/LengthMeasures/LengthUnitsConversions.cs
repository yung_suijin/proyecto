﻿using Library;
using System;
using System.Windows.Forms;

namespace Proyecto.LengthMeasures
{
    public partial class LengthUnitsConversions : Form
    {
        private readonly WindowEvents windowEvents;

        public LengthUnitsConversions()
        {
            InitializeComponent();

            windowEvents = new WindowEvents();
        }

        private void btnBeforeKilometers_Click(object sender, EventArgs e)
        {
            windowEvents.CloseCurrentWindowAndOpenNextWindow(this, new LengthKilometers());
        }

        private void btnNextLengthConversions_Click(object sender, EventArgs e)
        {
            windowEvents.CloseCurrentWindowAndOpenNextWindow(this, new LengthUnitsGame());
        }
    }
}