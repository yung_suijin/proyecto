﻿using Library;
using System;
using System.Windows.Forms;

namespace Proyecto.LengthMeasures
{
    public partial class LengthMeters : Form
    {
        private readonly WindowEvents windowEvents;

        public LengthMeters()
        {
            InitializeComponent();

            windowEvents = new WindowEvents();
        }

        private void btnBeforeLengths_Click(object sender, EventArgs e)
        {
            windowEvents.CloseCurrentWindowAndOpenNextWindow(this, new Lengths());
        }

        private void btnNextLengthCentimeters_Click(object sender, EventArgs e)
        {
            windowEvents.CloseCurrentWindowAndOpenNextWindow(this, new LengthCentimeters());
        }
    }
}