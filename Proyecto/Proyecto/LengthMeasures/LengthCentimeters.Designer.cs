﻿
namespace Proyecto.LengthMeasures
{
    partial class LengthCentimeters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LengthCentimeters));
            this.imgLength7 = new System.Windows.Forms.PictureBox();
            this.imgLength8 = new System.Windows.Forms.PictureBox();
            this.btnNextLengthKilometers = new System.Windows.Forms.Button();
            this.btnBeforeLengthMeters = new System.Windows.Forms.Button();
            this.imgLength9 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength9)).BeginInit();
            this.SuspendLayout();
            // 
            // imgLength7
            // 
            this.imgLength7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength7.Image = global::Proyecto.Properties.Resources.CentimeterInfo;
            this.imgLength7.Location = new System.Drawing.Point(24, 12);
            this.imgLength7.Name = "imgLength7";
            this.imgLength7.Size = new System.Drawing.Size(305, 297);
            this.imgLength7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength7.TabIndex = 2;
            this.imgLength7.TabStop = false;
            // 
            // imgLength8
            // 
            this.imgLength8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength8.Image = global::Proyecto.Properties.Resources.CentimeterRuler;
            this.imgLength8.Location = new System.Drawing.Point(355, -1);
            this.imgLength8.Name = "imgLength8";
            this.imgLength8.Size = new System.Drawing.Size(351, 323);
            this.imgLength8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength8.TabIndex = 3;
            this.imgLength8.TabStop = false;
            // 
            // btnNextLengthKilometers
            // 
            this.btnNextLengthKilometers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnNextLengthKilometers.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNextLengthKilometers.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNextLengthKilometers.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnNextLengthKilometers.Location = new System.Drawing.Point(597, 355);
            this.btnNextLengthKilometers.Name = "btnNextLengthKilometers";
            this.btnNextLengthKilometers.Size = new System.Drawing.Size(109, 60);
            this.btnNextLengthKilometers.TabIndex = 10;
            this.btnNextLengthKilometers.Text = "Next";
            this.btnNextLengthKilometers.UseVisualStyleBackColor = false;
            this.btnNextLengthKilometers.Click += new System.EventHandler(this.btnNextLengthKilometers_Click);
            // 
            // btnBeforeLengthMeters
            // 
            this.btnBeforeLengthMeters.BackColor = System.Drawing.Color.LightGreen;
            this.btnBeforeLengthMeters.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBeforeLengthMeters.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBeforeLengthMeters.Font = new System.Drawing.Font("Baloo", 15F);
            this.btnBeforeLengthMeters.Location = new System.Drawing.Point(471, 355);
            this.btnBeforeLengthMeters.Name = "btnBeforeLengthMeters";
            this.btnBeforeLengthMeters.Size = new System.Drawing.Size(109, 60);
            this.btnBeforeLengthMeters.TabIndex = 9;
            this.btnBeforeLengthMeters.Text = "Before";
            this.btnBeforeLengthMeters.UseVisualStyleBackColor = false;
            this.btnBeforeLengthMeters.Click += new System.EventHandler(this.btnBeforeLengthMeters_Click);
            // 
            // imgLength9
            // 
            this.imgLength9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgLength9.Image = global::Proyecto.Properties.Resources.CentimetersParts;
            this.imgLength9.Location = new System.Drawing.Point(12, 328);
            this.imgLength9.Name = "imgLength9";
            this.imgLength9.Size = new System.Drawing.Size(436, 110);
            this.imgLength9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLength9.TabIndex = 11;
            this.imgLength9.TabStop = false;
            // 
            // LengthCentimeters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Proyecto.Properties.Resources.wall5;
            this.ClientSize = new System.Drawing.Size(731, 450);
            this.Controls.Add(this.imgLength9);
            this.Controls.Add(this.btnNextLengthKilometers);
            this.Controls.Add(this.btnBeforeLengthMeters);
            this.Controls.Add(this.imgLength8);
            this.Controls.Add(this.imgLength7);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LengthCentimeters";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Length Units";
            ((System.ComponentModel.ISupportInitialize)(this.imgLength7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLength9)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox imgLength7;
        private System.Windows.Forms.PictureBox imgLength8;
        private System.Windows.Forms.Button btnNextLengthKilometers;
        private System.Windows.Forms.Button btnBeforeLengthMeters;
        private System.Windows.Forms.PictureBox imgLength9;
    }
}