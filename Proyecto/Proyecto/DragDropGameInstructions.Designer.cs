﻿
namespace Proyecto
{
    partial class DragDropGameInstructions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DragDropGameInstructions));
            this.lblTitleLengthExercises = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.AnswerBoxRing = new System.Windows.Forms.PictureBox();
            this.imgRing = new System.Windows.Forms.PictureBox();
            this.imgGram = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxRing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgRing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgGram)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitleLengthExercises
            // 
            this.lblTitleLengthExercises.AutoSize = true;
            this.lblTitleLengthExercises.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTitleLengthExercises.Font = new System.Drawing.Font("Baloo", 16F);
            this.lblTitleLengthExercises.Location = new System.Drawing.Point(12, 9);
            this.lblTitleLengthExercises.Name = "lblTitleLengthExercises";
            this.lblTitleLengthExercises.Size = new System.Drawing.Size(450, 70);
            this.lblTitleLengthExercises.TabIndex = 25;
            this.lblTitleLengthExercises.Text = "Select the measure unit and drag it into the \r\nblank rectangle that is below ever" +
    "y image.";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox1.Image = global::Proyecto.Properties.Resources.RedArrow;
            this.pictureBox1.Location = new System.Drawing.Point(237, 205);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(113, 105);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            // 
            // AnswerBoxRing
            // 
            this.AnswerBoxRing.BackColor = System.Drawing.Color.White;
            this.AnswerBoxRing.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AnswerBoxRing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AnswerBoxRing.Location = new System.Drawing.Point(54, 271);
            this.AnswerBoxRing.Name = "AnswerBoxRing";
            this.AnswerBoxRing.Size = new System.Drawing.Size(168, 39);
            this.AnswerBoxRing.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AnswerBoxRing.TabIndex = 42;
            this.AnswerBoxRing.TabStop = false;
            this.AnswerBoxRing.Tag = "Gram";
            // 
            // imgRing
            // 
            this.imgRing.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgRing.Image = global::Proyecto.Properties.Resources.Ring;
            this.imgRing.Location = new System.Drawing.Point(54, 96);
            this.imgRing.Name = "imgRing";
            this.imgRing.Size = new System.Drawing.Size(168, 160);
            this.imgRing.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgRing.TabIndex = 41;
            this.imgRing.TabStop = false;
            // 
            // imgGram
            // 
            this.imgGram.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.imgGram.BackgroundImage = global::Proyecto.Properties.Resources.Gram;
            this.imgGram.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.imgGram.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgGram.Location = new System.Drawing.Point(278, 162);
            this.imgGram.Name = "imgGram";
            this.imgGram.Size = new System.Drawing.Size(142, 33);
            this.imgGram.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgGram.TabIndex = 40;
            this.imgGram.TabStop = false;
            this.imgGram.Tag = "";
            // 
            // DragDropGameInstructions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Proyecto.Properties.Resources.wall11;
            this.ClientSize = new System.Drawing.Size(465, 339);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.AnswerBoxRing);
            this.Controls.Add(this.imgRing);
            this.Controls.Add(this.imgGram);
            this.Controls.Add(this.lblTitleLengthExercises);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DragDropGameInstructions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Game Instructions";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnswerBoxRing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgRing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgGram)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitleLengthExercises;
        private System.Windows.Forms.PictureBox AnswerBoxRing;
        private System.Windows.Forms.PictureBox imgRing;
        private System.Windows.Forms.PictureBox imgGram;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}